package ru.MrSinkaaa.TNTGames.points;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;

import java.util.HashMap;
import java.util.Map;

/*
public class PointsManager extends Manager {

    private PointsDataManager dataManager;

    public PointsManager(LollipopTNTGames plugin) {
        super(plugin);
        dataManager = new PointsdataManager(this);
    }

    @Override
    public void onEnable() {
        plugin.enableManager(dataManager);
    }

    @Override
    public void onDisable() {
        plugin.disableManager(PointsDataManager.class);
    }

    //func

    */
/**
     * Calculate final points for each player for that game and
     * store them in database. It also prepares(resets) data for the next game.
     *
     * @param game game
     *//*

    public Map<String, Long> calculateAndStorePoints(Game game, GamePlayer player) {
        var pointsRootConfig = plugin.config().points;
        var pointsModeConfig = pointsRootConfig.modes.get(game.getSettings().getType());

        //filter : no settings for that mode
        if(pointsModeConfig == null) return new HashMap<>();

        //calculate
        int pointsPerWin = pointsModeConfig.pointsPerWin;
        int maxAdditionalPoints = pointsModeConfig.maxAdditionalPoints;

        class GroupedPointsResult {
            private HashMap<String, Long> playerToPointsMap = new HashMap<>();
            private long sum;
        }

        Map<String, Long> collectedPointsMap = new HashMap<>();

        game.getPlayers().forEach(player -> {
            //transform to {action -> {player -> points, sum}} map
            var actionToGroupPointsResult = new HashMap<PointsAction, GroupedPointsResult>();
            if(player.isQuitWhenAlive()) return;

            //do
            var data = player.getCustomData(PointsData.class);
            data.getPointsPerAction().forEach((action, points) -> {
                action
            })
        });
    }
}
*/
