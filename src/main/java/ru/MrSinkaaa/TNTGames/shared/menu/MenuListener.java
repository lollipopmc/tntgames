package ru.MrSinkaaa.TNTGames.shared.menu;

import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;

import java.util.Optional;

public class MenuListener extends BukkitListener {

    public MenuListener(LollipopTNTGames plugin) {
        super(plugin);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        getMenu(e.getInventory()).ifPresent(m -> m.onClick(e));
    }

    @EventHandler
    public void onInventoryDrag(InventoryDragEvent e) {
        getMenu(e.getInventory()).ifPresent(m -> m.onDrag(e));
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        getMenu(e.getInventory()).ifPresent(m -> m.onClose(e));
    }

    //private
    private Optional<Menu> getMenu(Inventory inventory) {
        if(inventory != null && inventory.getHolder() instanceof Menu)
            return Optional.of((Menu) inventory.getHolder());
        else return Optional.empty();
    }
}
