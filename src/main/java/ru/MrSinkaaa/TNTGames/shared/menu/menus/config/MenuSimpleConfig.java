package ru.MrSinkaaa.TNTGames.shared.menu.menus.config;

import org.bukkit.configuration.ConfigurationSection;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.shared.item.ItemTemplate;
import ru.MrSinkaaa.TNTGames.shared.menu.menus.config.buttons.SimpleButton;

import java.util.HashMap;
import java.util.Map;

public class MenuSimpleConfig extends MenuConfig {

    public String title;
    public int lines;
    public Map<String, SimpleButton> buttons;

    public MenuSimpleConfig(LollipopTNTGames plugin, String id) {
        super(plugin, id);

        title = config.getString("title");
        lines = config.getInt("lines", 6);
        loadButtons();
    }

    //private

    protected void loadButtons() {
        buttons = new HashMap<>();
        ConfigurationSection sectionButtonList = config.getConfigurationSection("buttons");
        if(sectionButtonList != null) {
            for(String buttonId : sectionButtonList.getKeys(false)) {
                ConfigurationSection sectionButton = sectionButtonList.getConfigurationSection(buttonId);
                SimpleButton button = loadButtons(buttonId, sectionButton);
                buttons.put(buttonId, button);
            }
        }
    }

    protected SimpleButton loadButtons(String id, ConfigurationSection section) {
        ItemTemplate itemTemplate = ItemTemplate.fromConfig(section.getConfigurationSection("item"));
        int slot = readSlot(section);
        return new SimpleButton(itemTemplate, slot);
    }

    protected int readSlot(ConfigurationSection section) {
        if(section.contains("position")) {
            String[] raw = section.getString("position").split(" ");
            int x = Integer.parseInt(raw[0]);
            int y = Integer.parseInt(raw[1]);
            return (x - 1) + (y - 1) * 9;
        } else return section.getInt("slot", 0);
    }
}
