package ru.MrSinkaaa.TNTGames.shared.command.commands;

import org.bukkit.command.CommandSender;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Permission;
import ru.MrSinkaaa.TNTGames.shared.command.Cmd;
import ru.MrSinkaaa.TNTGames.shared.command.commands.tntgames.SubCmdSetup;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ua.i0xhex.lib.command.CommandManager;

/**
 *
 * @author MrSinkaaa
 */
public class CmdTNTGames extends Cmd {

    public CmdTNTGames(LollipopTNTGames plugin, CommandManager<CommandSender> manager) {
        super(plugin, manager, "tntgames", "tr");
    }
    
    @Override
    public void construct() {
        builder = builder.permission(Permission.COMMAND_TNTGAMES);
        
        constructStart();
        constructStop();
        constructSetup();
    }

    //subcommands
    public void constructStart() {
        //tr start
        manager.command(builder.literal("start").handler(context -> {
            //base
            Lang lang = plugin.lang();
            CommandSender sender = context.getSender();

            //check : already running
            GameManager manager = plugin.getManager(GameManager.class);
            if(manager.getCurrentGame() != null) {
                lang.sendMessage(sender, "chat.command.tntgames.start.already");
                return;
            }

            //do
            lang.sendMessage(sender, "chat.command.tntgames.start.queue");
            manager.startNextGame();
        }));
    }
    
    public void constructStop() {
        //tr stop
        manager.command(builder.literal("stop").handler(context -> {
            //base
            Lang lang = plugin.lang();
            CommandSender sender = context.getSender();

            //check : not running
            GameManager manager = plugin.getManager(GameManager.class);
            if(manager.getCurrentGame() == null) {
                lang.sendMessage(sender, "chat.command.tntgames.stop.not-running");
                return;
            }

            //do
            lang.sendMessage(sender, "chat.command.tntgames.stop.queue");
            manager.stopCurrentGame();
        }));
    }

    public void constructSetup() {
        new SubCmdSetup(plugin, manager, builder);
    }

}
