/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.MrSinkaaa.TNTGames.shared.command.commands;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.shared.command.Cmd;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.misc.GameType;
import ua.i0xhex.lib.command.CommandManager;
import ua.i0xhex.sync.client.bukkit.LollipopSyncClient;
import ua.i0xhex.sync.client.bukkit.modules.minigame.MinigameModule;
import ua.i0xhex.sync.client.bukkit.modules.minigame.info.server.MinigameServer;
import ua.i0xhex.sync.client.bukkit.modules.minigame.info.server.MinigameServerState;

import java.util.Comparator;
import java.util.List;

/**
 *
 * @author MrSinkaaa
 */
public class CmdNextGame extends Cmd {

    public CmdNextGame(LollipopTNTGames plugin, CommandManager<CommandSender> manager) {
        super(plugin, manager, "nextgame");
    }

    @Override
    public void construct() {
        builder = builder.senderType(Player.class);

        manager.command(builder
              .handler(context -> {
                  //base
                  var lang = plugin.lang();
                  var player = plugin.getPlayer((Player) context.getSender());

                  //check : game not running
                  var gameManager = plugin.getManager(GameManager.class);
                  if(!gameManager.isGameStarted()) {
                      lang.sendMessage(player, "chat.command.nextgame.not-running");
                      return;
                  }

                  var game = gameManager.getCurrentGame();
                  var gameType = game.getSettings().getType();

                  //check : not spectator
                  if(!player.isSpectator()) {
                      lang.sendMessage(player, "chat.command.nextgame.not-spectator");
                      return;
                  }

                  //check : sync not supported
                  if(!Bukkit.getPluginManager().isPluginEnabled("LollipopSyncClient")) {
                      lang.sendMessage(player, "chat.command._.sync-error",
                            new StaticReplacer()
                                  .set("error", "Not supported."));
                      return;
                  }

                  //do
                  var minigameApi = LollipopSyncClient.get().module(MinigameModule.class).getApi();
                  minigameApi.getServersInfo(List.of("tr-!"))
                        .thenAccept(result -> {
                            if(result.isSuccess()) {
                                var server = result.getValue().values()
                                      .stream().filter(s -> s.getState() == MinigameServerState.WAITING)
                                      .filter(s -> GameType.get(s.getMode()) == gameType)
                                      .filter(s -> s.getPlayersOnline() < s.getPlayersMax())
                                      .max(Comparator.comparingInt(MinigameServer::getPlayersOnline))
                                      .orElse(null);

                                if(server != null) {
                                    var id = server.getId();
                                    lang.sendMessage(player, "chat.command.nextgame.done",
                                          new StaticReplacer().set("server", id));
                                    transferPlayer(player, id);
                                } else {
                                    lang.sendMessage(player,"chat.command.nextgame.not-found");
                                }
                            } else {
                                lang.sendMessage(player, "chat.command._.sync-error",
                                      new StaticReplacer()
                                            .set("error", result.getMessage()));
                            }
                        });
              }));
    }

    @SuppressWarnings("UnstableApiUsage")
    private void transferPlayer(GamePlayer player, String server) {
        ByteArrayDataOutput out = ByteStreams.newDataOutput();
        out.writeUTF("Connect");
        out.writeUTF(server);
        player.sendPluginMessage(plugin, "BungeeCord", out.toByteArray());
    }
    
}
