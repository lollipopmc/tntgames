package ru.MrSinkaaa.TNTGames.shared.command.commands.tntgames;


import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.model.BlockPosition;
import ru.MrSinkaaa.TNTGames.common.model.Position;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.shared.command.SubCmd;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameSettings;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameSettingsValidation;
import ua.i0xhex.lib.command.Command;
import ua.i0xhex.lib.command.CommandManager;
import ua.i0xhex.lib.command.arguments.CommandArgument;
import ua.i0xhex.lib.command.arguments.parser.ArgumentParseResult;
import ua.i0xhex.lib.command.context.CommandContext;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

public class SubCmdSetup extends SubCmd {

    public SubCmdSetup(LollipopTNTGames plugin, CommandManager<CommandSender> manager,
                       Command.Builder<CommandSender> builder) {
        super(plugin, manager, builder);
    }

    @Override
    public void construct() {
        //arguments
        CommandArgument<CommandSender, String> gameArg = CommandArgument.<CommandSender, String>ofType(String.class, "game")
              .withSuggestionsProvider((c,s) -> {
                  GameManager gameManager = plugin.getManager(GameManager.class);

                  List<String> result = new ArrayList<>();
                  result.addAll(gameManager.getTemporaryGameSettings().keySet());
                  result.addAll(gameManager.getActiveGameSettings().keySet());
                  return result;
              })
              .withParser((sender, args) -> {
                  String result = args.poll().toLowerCase();
                  return ArgumentParseResult.success(result);
              }).build();

        //parent
        builder = builder.senderType(Player.class).literal("setup" ).argument(gameArg);

        //сhild
        constructCreate();
        constructRemove();
        constructSave();
        constructCancel();

        constructPos();
        constructLobby();
        constructSpawnLocations();
    }

    // preprocess

    private void handleSetupEdit(CommandContext<CommandSender> context,
                                 Consumer<GameSettings> consumer) {
        Lang lang = plugin.lang();
        Player sender = (Player) context.getSender();
        GameManager gameManager = plugin.getManager(GameManager.class);

        // check : game not found
        String gameId = context.get("game");
        GameSettings settings = gameManager.editGameSettings(gameId);
        if (settings == null) {
            lang.sendMessage(sender,
                  "chat.command.tntgames.setup.game-not-found",
                  new StaticReplacer()
                        .set("game", gameId));
            return;
        }

        // check : game currently running
        Game game = gameManager.getCurrentGame();
        if (game != null && game.getSettings().getId().equals(gameId)) {
            lang.sendMessage(sender,
                  "chat.command.tntgames.setup.game-running",
                  new StaticReplacer()
                        .set("game", gameId));
            return;
        }

        // do
        consumer.accept(settings);
    }

    //subcommands
    private void constructCreate() {
        //tr setup <game> create
        manager.command(builder.literal("create").handler(context -> {
            //base
            Lang lang = plugin.lang();
            Player sender = (Player) context.getSender();


            //context
            String gameId = context.get("game");

            //check : already exists
            GameManager gameManager = plugin.getManager(GameManager.class);
            if(gameManager.getTemporaryGameSettings().containsKey(gameId)
            || gameManager.getActiveGameSettings().containsKey(gameId)) {
                lang.sendMessage(sender, "chat.command.tntgames.setup.create.already-exists",
                      new StaticReplacer().set("game", gameId));
                return;
            }

            //do
            gameManager.createGameSettingsEdit(gameId,sender.getWorld().getName());
            lang.sendMessage(sender,"chat.command.tntgames.setup.create.done",
                  new StaticReplacer().set("game",gameId));
        }));
    }

    private void constructRemove() {
        //tr setup <game> remove
        manager.command(builder.literal("remove").handler(context -> {
            //base
            Lang lang = plugin.lang();
            Player sender = (Player) context.getSender();

            //context
            String gameId = context.get("game");

            //check : game does not exist
            GameManager gameManager = plugin.getManager(GameManager.class);
            if(!gameManager.getActiveGameSettings().containsKey(gameId)
                && !gameManager.getTemporaryGameSettings().containsKey(gameId)) {
                lang.sendMessage(sender,"chat.command.tntgames.setup.remove.does-not-exists",
                      new StaticReplacer().set("game", gameId));
            }

            //do
            gameManager.removeGameSettings(gameId);
            lang.sendMessage(sender,"chat.command.tntgames.setup.remove.done",
                  new StaticReplacer().set("game", gameId));
        }));
    }

    private void constructSave() {
        //tr setup <game> save
        manager.command(builder.literal("save").handler(context -> {
            //base
            Lang lang = plugin.lang();
            Player sender = (Player) context.getSender();

            //context
            String gameId = context.get("game");

            //check : game is not in edit mode
            GameManager gameManager = plugin.getManager(GameManager.class);
            GameSettings settings = gameManager.getTemporaryGameSettings().get(gameId);
            if(settings == null) {
                lang.sendMessage(sender, "chat.command.tntgames.setup.save.not-in-edit-mode",
                      new StaticReplacer().set("game", gameId));
                return;
            }

            //check : validation failed
            GameSettingsValidation validation = settings.validate();
            if(!validation.isPassed()) {
                lang.sendMessage(sender,"chat.notify.validation.syntax.header",
                      new StaticReplacer().set("game",gameId));

                int amountToShow = 5;
                int amountLeft = validation.getEntries().size() - amountToShow;
                Iterator<GameSettingsValidation.Entry> entriesIt = validation.getEntries().iterator();

                int i = 0;
                while(entriesIt.hasNext() && i++ < amountToShow) {
                    String message = entriesIt.next().getMessage();
                    lang.sendMessage(sender,"chat.notify.validation.syntax.line",
                          new StaticReplacer().set("message",message));
                }

                if(amountLeft > 0) {
                    lang.sendMessage(sender,"chat.notify.validation.syntax.footer",
                          new StaticReplacer().set("amount_left",amountLeft));
                }
                return;
            }

            //do
            settings.write();
            gameManager.saveGameSettingsEdit(gameId);
            lang.sendMessage(sender,"chat.command.tntgames.setup.save.done",
                  new StaticReplacer().set("game",gameId));
        }));
    }

    private void constructCancel() {
        //tr setup <game> cancel
        manager.command(builder.literal("cancel").handler(context -> {
            //base
            Lang lang = plugin.lang();
            Player sender = (Player) context.getSender();

            //context
            String gameId = context.get("game");

            //check : game is not in edit mode
            GameManager gameManager = plugin.getManager(GameManager.class);
            GameSettings settings = gameManager.getTemporaryGameSettings().get(gameId);
            if(settings == null) {
                lang.sendMessage(sender,"chat.command.tntgames.setup.not-in-edit-mode",
                      new StaticReplacer().set("game",gameId));
                return;
            }

            //do
            gameManager.cancelGameSettingsEdit(gameId);
            lang.sendMessage(sender, "chat.command.tntgames.setup.cancel.done",
                  new StaticReplacer().set("game",gameId));
        }));
    }

    private void constructPos() {
        //tr setup <game> pos1
        manager.command(builder.literal("pos1").handler(context ->
              handleSetupEdit(context,settings -> {
                  //base
                  Lang lang = plugin.lang();
                  Player sender = (Player) context.getSender();

                  //do
                  BlockPosition position = BlockPosition.fromLocation(sender.getLocation());
                  settings.getBounds()[0] = position;

                  String positionName = toString(position);
                  lang.sendMessage(sender,"chat.command.tntgames.setup.pos1.done",
                        new StaticReplacer().set("position",positionName));
              })));

        //tr setup <game> pos2
        manager.command(builder.literal("pos2").handler(context ->
              handleSetupEdit(context,settings -> {
                  //base
                  Lang lang = plugin.lang();
                  Player sender = (Player) context.getSender();

                  //do
                  BlockPosition position = BlockPosition.fromLocation(sender.getLocation());
                  settings.getBounds()[1] = position;

                  String positionName = toString(position);
                  lang.sendMessage(sender,"chat.command.tntgames.setup.pos2.done",
                        new StaticReplacer().set("position",positionName));
              })));
    }

    private void constructLobby() {
        //tr setup <game> lobby spawn
        manager.command(builder.literal("lobby").literal("spawn")
              .handler(context ->  handleSetupEdit(context,settings -> {
                  //base
                  Lang lang = plugin.lang();
                  Player sender = (Player) context.getSender();

                  //do
                  Position position = Position.fromLocation(sender.getLocation());
                  position = MiscUtils.normalizePosition(position);

                  settings.getLobby().setSpawn(position);
                  lang.sendMessage(sender,"chat.command.tntgames.setup.lobby.spawn.done");
              })));
    }

    private void constructSpawnLocations() {
        //tr setup <game> spawn add
        manager.command(builder.literal("spawn").literal("add")
              .handler(context ->  handleSetupEdit(context,settings -> {
                  //base
                  Lang lang = plugin.lang();
                  Player sender = (Player) context.getSender();

                  //do
                  Position position = Position.fromLocation(sender.getLocation());
                  position = MiscUtils.normalizePosition(position);

                  settings.getSpawnSettings().addSpawn(position);
                  lang.sendMessage(sender,"chat.command.tntgames.setup.spawns.add");
              })));

        //tr setup <game> spawn remove
        manager.command(builder.literal("spawn").literal("remove")
              .handler(context ->  handleSetupEdit(context,settings -> {
                  //base
                  Lang lang = plugin.lang();
                  Player sender = (Player) context.getSender();

                  //do
                  Position position = Position.fromLocation(sender.getLocation());
                  position = MiscUtils.normalizePosition(position);

                  settings.getSpawnSettings().removeSpawn();
                  lang.sendMessage(sender,"chat.command.tntgames.setup.spawns.remove");
              })));
    }

    // private

    private String toString(Position position) {
        return String.format("%d %d %d %d %d",
              (int) position.getX(), (int) position.getY(), (int) position.getZ(),
              (int) position.getYaw(), (int) position.getPitch());
    }

    private String toString(BlockPosition position) {
        return String.format("%d %d %d",
              position.getX(), position.getY(), position.getZ());
    }
}
