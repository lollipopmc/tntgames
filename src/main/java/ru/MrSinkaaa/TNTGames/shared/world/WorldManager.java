package ru.MrSinkaaa.TNTGames.shared.world;

import org.bukkit.*;
import org.bukkit.entity.Entity;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.common.util.FileUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashSet;
import java.util.Set;

public class WorldManager extends Manager {

    private Set<String> loadedWorlds;

    public WorldManager(LollipopTNTGames plugin) {
        super(plugin);
        loadedWorlds = new HashSet<>();
    }

    @Override
    public void onEnable() {
        super.onEnable();
    }

    // func

    /**
     * Load world from gameworlds/ directory and set game specific settings.
     * This first creates a world copy in world directory.
     *
     * @param worldName world name
     * @return world instance or null if world not loaded
     */
    public World load(String worldName) {
        try {
            Path serverRootDir = plugin.getDataFolder().toPath().toAbsolutePath().getParent().getParent();
            Path worldContainerDir = Bukkit.getWorldContainer().toPath().toAbsolutePath();
            Path worldBackupDir = serverRootDir
                  .resolve(plugin.config().backupWorldsPath)
                  .resolve(worldName);

            // check : world directory not found
            if (!Files.isDirectory(worldBackupDir))
                return null;

            // copy world
            Path worldDir = worldContainerDir.resolve(worldName);
            FileUtils.copyDirectory(worldBackupDir, worldDir, StandardCopyOption.REPLACE_EXISTING);

            // load
            return loadFromRoot(worldName);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Load world from root directory and set game specific settings.
     *
     * @param worldName world name
     * @return world instance or null if world not loaded
     */
    public World loadFromRoot(String worldName) {
        try {
            // load
            WorldCreator creator = new WorldCreator(worldName);
            creator.environment(World.Environment.NORMAL);
            creator.type(WorldType.FLAT);
            creator.generateStructures(false);
            creator.generator(new VoidChunkGenerator());

            World world = Bukkit.createWorld(creator);
            world.setAutoSave(false);
            world.setDifficulty(Difficulty.HARD);
            world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
            world.setGameRule(GameRule.DO_IMMEDIATE_RESPAWN, true);
            world.setGameRule(GameRule.DO_MOB_SPAWNING, false);
            world.setGameRule(GameRule.DO_ENTITY_DROPS, false);
            world.setGameRule(GameRule.DO_MOB_LOOT, false);
            world.setGameRule(GameRule.DO_INSOMNIA, false);
            world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
            world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
            world.setGameRule(GameRule.SPECTATORS_GENERATE_CHUNKS, false);

            loadedWorlds.add(worldName);
            return world;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Unload world and delete world directory
     *
     * @param world world to unload
     */
    public void unload(World world) {
        // remove all entities
        world.getEntities().forEach(Entity::remove);

        // unload world
        Bukkit.getServer().unloadWorld(world, false);

        // delete world directory
        Path dir = world.getWorldFolder().toPath();
        FileUtils.deleteDirectory(dir);

        loadedWorlds.remove(world.getName());
    }

    public boolean isLoaded(String worldName) {
        return loadedWorlds.contains(worldName);
    }
}
