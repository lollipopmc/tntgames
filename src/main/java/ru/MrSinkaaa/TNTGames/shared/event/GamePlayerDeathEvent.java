package ru.MrSinkaaa.TNTGames.shared.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;

public class GamePlayerDeathEvent extends Event {

    private GamePlayer player;

    public GamePlayerDeathEvent(GamePlayer player) {
        this.player = player;
    }

    public GamePlayer getPlayer() {
        return player;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    // static

    private static final HandlerList HANDLERS = new HandlerList();

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
