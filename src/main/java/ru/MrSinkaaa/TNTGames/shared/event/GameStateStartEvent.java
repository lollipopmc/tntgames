package ru.MrSinkaaa.TNTGames.shared.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import ru.MrSinkaaa.TNTGames.shared.game.GameState;

public class GameStateStartEvent extends Event {

    private GameState state;

    private static final HandlerList HANDLERS = new HandlerList();

    public GameStateStartEvent(GameState state) {
        this.state = state;
    }

    //getters
    public GameState getState() {
        return state;
    }

    @Override
    public HandlerList getHandlers() {
        return HANDLERS;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }
}
