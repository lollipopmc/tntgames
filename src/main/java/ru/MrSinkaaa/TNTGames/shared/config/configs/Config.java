package ru.MrSinkaaa.TNTGames.shared.config.configs;

import org.bukkit.Sound;
import ru.MrSinkaaa.TNTGames.common.model.Position;
import ru.MrSinkaaa.TNTGames.shared.config.ConfigLoader;
import ru.MrSinkaaa.TNTGames.shared.config.ConfigObject;
import ru.MrSinkaaa.TNTGames.shared.game.misc.GameType;
import ru.MrSinkaaa.TNTGames.shared.game.misc.Layer;
import ua.i0xhex.lib.configurate.NodePath;
import ua.i0xhex.lib.configurate.objectmapping.ConfigSerializable;
import ua.i0xhex.lib.configurate.transformation.ConfigurationTransformation;
import ua.i0xhex.lib.geantyref.TypeToken;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

@ConfigSerializable
public class Config extends ConfigObject {

    public boolean startRandomGame;
    public boolean useFawe;
    public MysqlSection mysql;
    public String backupWorldsPath;
    public Position limbo;
    public Map<String, Integer> lobbyItems;
    public Map<String, Integer> spectatorItems;
    public int minutesPerGame;
    public Map<String, Sound> sounds;
    public RewardSection reward;
//    public PointsSection points;
//    public LayersSection layers;

    public Set<String> debug;

    private Config() {
        startRandomGame = false;
        useFawe = true;
        mysql = new MysqlSection();
        mysql.url = "127.0.0.1:3306/dbname?useSSL=false";
        mysql.username = "username";
        mysql.password = "password";
        backupWorldsPath = "worldsaves";
        limbo = new Position("limbo", 0, 50, 0, 0, 0);
        minutesPerGame = 15;
        lobbyItems = new LinkedHashMap<>();
        lobbyItems.put("capsule_select", 1);
        lobbyItems.put("rating", 3);
        lobbyItems.put("base_cosmetics", 5);
        lobbyItems.put("base,quit", 7);
        spectatorItems = new LinkedHashMap<>();
        spectatorItems.put("spectator_menu", 1);
        spectatorItems.put("spectator_speed", 3);
        spectatorItems.put("base_cosmetics", 5);
        spectatorItems.put("base_quit", 7);
//        this.layers = new LayersSection();
        reward = new RewardSection();
        sounds = new LinkedHashMap<>();
        sounds.put("waiting-timer-start", Sound.ENTITY_EXPERIENCE_ORB_PICKUP);
        debug = new LinkedHashSet<>();
        debug.add("state");
    }


    @ConfigSerializable
    public static class MysqlSection {
        public String url;
        public String username;
        public String password;

        private MysqlSection() {}
    }

    @ConfigSerializable
    public static class RewardSection {

        public Map<GameType, Double> amountPerMode;
        public boolean proportionalToPoints;

        private RewardSection() {
            amountPerMode = new LinkedHashMap<>();
            amountPerMode.put(GameType.COMMON, 300D);
            amountPerMode.put(GameType.PVP, 300D);
            amountPerMode.put(GameType.BOW, 300D);

            proportionalToPoints = true;
        }
    }

//    @ConfigSerializable
//    public static class LayersSection {
//
//        public Map<Integer, Layer> layers;
//
//        private LayersSection() {
//            layers = new LinkedHashMap<>();
//        }
//    }

/*    @ConfigSerializable
    public static class PointsSection {

        public Map<GameType, ModeSection> modes;

        private PointsSection() {
            modes = new LinkedHashMap<>();
            initCommonMode();
            initPVPMode();
            initBowMode();
        }

        private void initCommonMode() {
            var section = new ModeSection(150, 105);

            {
*//*                var action = new PointsActionLastAlive(1);
                action.setStandardProperties(0.5, OptInt.of(4), OptInt.of(2));
                section.addAction(action);*//*
            }
            modes.put(GameType.COMMON, section);
        }

        private void initPVPMode() {
            var section = new ModeSection(250, 175);

            {
*//*                var action = new PointsActionLastAlive(1);
                action.setStandardProperties(0.5, OptInt.of(6), OptInt.of(3));
                section.addAction(action);*//*
            }
            modes.put(GameType.PVP, section);
        }

        private void initBowMode() {
            var section = new ModeSection(250, 150);

            {
*//*                var action = new PointsActionLastAlive(1);
                action.setStandardProperties(0.5, OptInt.of(6), OptInt.of(3));
                section.addAction(action);*//*
            }
            modes.put(GameType.BOW, section);
        }*/

        //sections

/*        @ConfigSerializable
        public static class ModeSection {

            public int pointsPerWin;
            public int maxAdditionalPoints;
            public Map<String, PointsAction> actions;

            private transient double actionsCoefficientSum = 0;

            private ModeSection() {
                this.actions = new LinkedHashMap<>();
            }

            private ModeSection(int pointsPerWin, int maxAdditionalPoints) {
                this();
                this.pointsPerWin = pointsPerWin;
                this.maxAdditionalPoints = maxAdditionalPoints;
            }

            //func
            public double getNormalizedCoefficient(String actionId) {
                if(actionsCoefficientSum == 0) {
                    actions.values().forEach(a ->
                          actionsCoefficientSum += a.getCoefficient());
                }

                var action = actions.get(actionId);
                if(action == null) return 0;
                else return action.getCoefficient() / actionsCoefficientSum;
            }

            //modifiers
            public void addAction(PointsAction action) {
                this.actions.put(action.getId(), action);
            }
        }
    }*/

    //static
    public static Config load() {
        return ConfigLoader.builder(Config.class,"config.yml").transformation(TRANSFORMATION)
              .build().load();
    }

    private static int LATEST_VERSION = 0;
    private static ConfigurationTransformation TRANSFORMATION;

    static {
        TRANSFORMATION = ConfigurationTransformation.versionedBuilder().versionKey("version")
              .addVersion(LATEST_VERSION, transformToLatest()).build();
    }

    private static ConfigurationTransformation transformToLatest() {
        return ConfigurationTransformation.builder()
              .addAction(NodePath.path("win-reward"),(path, node) -> {
                  double amount = node.getDouble();

                  //move this to per mode
                  var replacementNode = node.parent().node("reward");
                  var modeToAmountMap = new LinkedHashMap<GameType, Double>();
                  for(var type : GameType.values())
                      modeToAmountMap.put(type,amount);
                  replacementNode.node("amount-per-mode").set(new TypeToken<LinkedHashMap<GameType, Double>>() {},
                        modeToAmountMap);

                  //delete current node
                  node.set(null);
                  return null;
              }).build();
    }
}
