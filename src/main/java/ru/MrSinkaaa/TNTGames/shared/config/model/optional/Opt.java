package ru.MrSinkaaa.TNTGames.shared.config.model.optional;

public interface Opt {

    public boolean isPresent();
}
