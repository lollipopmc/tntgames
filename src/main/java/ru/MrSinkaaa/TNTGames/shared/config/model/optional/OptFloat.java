package ru.MrSinkaaa.TNTGames.shared.config.model.optional;

import java.util.Objects;

public class OptFloat implements Opt {

    private boolean present;
    private float value;

    private OptFloat() {}

    private OptFloat(float value, boolean present) {
        this.value = value;
        this.present = present;
    }

    // getters

    @Override
    public boolean isPresent() {
        return present;
    }

    public float get() {
        return value;
    }

    public float get(float def) {
        if (!present) return def;
        else return value;
    }

    // equality

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptFloat that)) return false;
        return present == that.present && Float.compare(that.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(present, value);
    }


    // static

    public static OptFloat of(float value) {
        return new OptFloat(value, true);
    }

    public static OptFloat empty() {
        return new OptFloat();
    }

    public static OptFloat empty(float def) {
        return new OptFloat(def, false);
    }
}
