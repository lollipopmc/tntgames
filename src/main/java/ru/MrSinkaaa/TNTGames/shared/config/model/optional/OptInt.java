package ru.MrSinkaaa.TNTGames.shared.config.model.optional;

import java.util.Objects;

public class OptInt implements Opt {

    private boolean present;
    private int value;

    private OptInt() {}

    private OptInt(int value, boolean present) {
        this.value = value;
        this.present = present;
    }

    // getters

    @Override
    public boolean isPresent() {
        return present;
    }

    public int get() {
        return value;
    }

    public int get(int def) {
        if (!present) return def;
        else return value;
    }

    // equality

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptInt that)) return false;
        return present == that.present && value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(present, value);
    }

    // static

    public static OptInt of(int value) {
        return new OptInt(value, true);
    }

    public static OptInt empty() {
        return new OptInt();
    }

    public static OptInt empty(int def) {
        return new OptInt(def, false);
    }
}

