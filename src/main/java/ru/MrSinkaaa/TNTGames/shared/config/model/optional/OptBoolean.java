package ru.MrSinkaaa.TNTGames.shared.config.model.optional;

import java.util.Objects;

public class OptBoolean implements Opt {

    private boolean present;
    private boolean value;

    private OptBoolean() {}

    private OptBoolean(boolean value, boolean present) {
        this.value = value;
        this.present = present;
    }

    // getters

    @Override
    public boolean isPresent() {
        return present;
    }

    public boolean get() {
        return value;
    }

    public boolean get(boolean def) {
        if (!present) return def;
        else return value;
    }

    // equality

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptBoolean that)) return false;
        return present == that.present && value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(present, value);
    }

    // static

    public static OptBoolean of(boolean value) {
        return new OptBoolean(value, true);
    }

    public static OptBoolean empty() {
        return new OptBoolean();
    }

    public static OptBoolean empty(boolean def) {
        return new OptBoolean(def, false);
    }
}
