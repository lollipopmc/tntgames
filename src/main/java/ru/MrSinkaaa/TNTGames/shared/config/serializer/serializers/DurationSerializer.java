package ru.MrSinkaaa.TNTGames.shared.config.serializer.serializers;

import java.lang.reflect.Type;
import java.time.Duration;

import org.checkerframework.checker.nullness.qual.Nullable;

import ua.i0xhex.lib.configurate.ConfigurationNode;
import ua.i0xhex.lib.configurate.serialize.SerializationException;
import ua.i0xhex.lib.configurate.serialize.TypeSerializer;

public class DurationSerializer implements TypeSerializer<Duration> {

    @Override
    public Duration deserialize(Type type, ConfigurationNode node)
          throws SerializationException {
        String str = node.getString();

        long millis = 0;
        StringBuilder buffer = new StringBuilder();
        for (char ch : str.toCharArray()) {
            if (ch >= '0' && ch <= '9') {
                buffer.append(ch);
            } else {
                long value = Integer.parseInt(buffer.toString());
                buffer.setLength(0);
                switch (ch) {
                    case 'h':
                        millis += value * (60 * 60 * 1000L);
                        break;
                    case 'm':
                        millis += value * (60 * 1000L);
                        break;
                    case 's':
                        millis += value * 1000L;
                        break;
                }
            }
        }

        return Duration.ofMillis(millis);
    }

    @Override
    public void serialize(Type type, @Nullable Duration obj, ConfigurationNode node)
          throws SerializationException {
        int hours = obj.toHoursPart();
        int minutes = obj.toMinutesPart();
        int seconds = obj.toSecondsPart();

        StringBuilder builder = new StringBuilder();
        if (hours > 0)
            builder.append(hours).append('h');
        if (minutes > 0)
            builder.append(minutes).append('m');
        if (seconds > 0 || builder.length() == 0)
            builder.append(seconds).append('s');

        node.set(builder.toString());
    }
}

