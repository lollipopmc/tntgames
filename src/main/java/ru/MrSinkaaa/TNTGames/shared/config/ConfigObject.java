package ru.MrSinkaaa.TNTGames.shared.config;

import ua.i0xhex.lib.configurate.CommentedConfigurationNode;
import ua.i0xhex.lib.configurate.yaml.YamlConfigurationLoader;

/**
 *
 * @author MrSinkaaa
 */
public abstract class ConfigObject {
    
    private transient YamlConfigurationLoader loader;
    private transient CommentedConfigurationNode rootNode;
    
    //subscription
    protected void onPostLoad() {}
    
    protected void onPostSave() {}
    
    //getters
    public YamlConfigurationLoader getLoader() {
        return loader;
    }
    
    public CommentedConfigurationNode getRootNode() {
        return rootNode;
    }
    
    //protected
    protected void setLoader(YamlConfigurationLoader loader) {
        this.loader = loader;
    }
    
    protected void setRootNode(CommentedConfigurationNode rootNode) {
        this.rootNode = rootNode;
    }
}
