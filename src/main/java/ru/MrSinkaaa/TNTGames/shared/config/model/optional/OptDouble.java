package ru.MrSinkaaa.TNTGames.shared.config.model.optional;

import java.util.Objects;

public class OptDouble implements Opt {

    private boolean present;
    private double value;

    private OptDouble() {}

    private OptDouble(double value, boolean present) {
        this.value = value;
        this.present = present;
    }

    // getters

    @Override
    public boolean isPresent() {
        return present;
    }

    public double get() {
        return value;
    }

    public double get(double def) {
        if (!present) return def;
        else return value;
    }

    // equality

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptDouble that)) return false;
        return present == that.present && Double.compare(that.value, value) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(present, value);
    }

    // static

    public static OptDouble of(double value) {
        return new OptDouble(value, true);
    }

    public static OptDouble empty() {
        return new OptDouble();
    }

    public static OptDouble empty(double def) {
        return new OptDouble(def, false);
    }
}
