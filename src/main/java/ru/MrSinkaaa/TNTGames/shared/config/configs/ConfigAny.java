package ru.MrSinkaaa.TNTGames.shared.config.configs;

import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.MemorySection;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.MrSinkaaa.TNTGames.shared.config.AbstractConfig;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ConfigAny extends AbstractConfig {

    public ConfigAny(JavaPlugin plugin, String name, boolean useResource) {
        super(plugin, name, useResource);
        load();
    }

    public ConfigAny(JavaPlugin plugin, String name, boolean useResource, char pathSeparator) {
        super(plugin, name, useResource, pathSeparator);
        load();
    }

    public ConfigAny(JavaPlugin plugin, File file) {
        super(plugin, file);
        load();
    }

    public ConfigAny(JavaPlugin plugin, File file, String resource) {
        super(plugin, file, resource);
        load();
    }

    // delegate

    public @NotNull Set<String> getKeys(boolean deep) {
        return config.getKeys(deep);
    }

    public @NotNull Map<String, Object> getValues(boolean deep) {
        return config.getValues(deep);
    }

    public boolean contains(@NotNull String path) {
        return config.contains(path);
    }

    public boolean contains(@NotNull String path, boolean ignoreDefault) {
        return config.contains(path, ignoreDefault);
    }

    public boolean isSet(@NotNull String path) {
        return config.isSet(path);
    }

    @NotNull
    public String getCurrentPath() {
        return config.getCurrentPath();
    }

    @NotNull
    public String getName() {
        return config.getName();
    }

    public @Nullable Configuration getRoot() {
        return config.getRoot();
    }

    @Nullable
    public ConfigurationSection getDefaultSection() {
        return config.getDefaultSection();
    }

    public void set(@NotNull String path, @Nullable Object value) {
        config.set(path, value);
    }

    @Nullable
    public Object get(@NotNull String path) {
        return config.get(path);
    }

    @Contract("_, !null -> !null")
    @Nullable
    public Object get(@NotNull String path, @Nullable Object def) {
        return config.get(path, def);
    }

    @NotNull
    public ConfigurationSection createSection(@NotNull String path) {
        return config.createSection(path);
    }

    @NotNull
    public ConfigurationSection createSection(@NotNull String path, @NotNull Map<?, ?> map) {
        return config.createSection(path, map);
    }

    @Nullable
    public String getString(@NotNull String path) {
        return config.getString(path);
    }

    @Contract("_, !null -> !null")
    @Nullable
    public String getString(@NotNull String path, @Nullable String def) {
        return config.getString(path, def);
    }

    public boolean isString(@NotNull String path) {
        return config.isString(path);
    }

    public int getInt(@NotNull String path) {
        return config.getInt(path);
    }

    public int getInt(@NotNull String path, int def) {
        return config.getInt(path, def);
    }

    public boolean isInt(@NotNull String path) {
        return config.isInt(path);
    }

    public boolean getBoolean(@NotNull String path) {
        return config.getBoolean(path);
    }

    public boolean getBoolean(@NotNull String path, boolean def) {
        return config.getBoolean(path, def);
    }

    public boolean isBoolean(@NotNull String path) {
        return config.isBoolean(path);
    }

    public double getDouble(@NotNull String path) {
        return config.getDouble(path);
    }

    public double getDouble(@NotNull String path, double def) {
        return config.getDouble(path, def);
    }

    public boolean isDouble(@NotNull String path) {
        return config.isDouble(path);
    }

    public long getLong(@NotNull String path) {
        return config.getLong(path);
    }

    public long getLong(@NotNull String path, long def) {
        return config.getLong(path, def);
    }

    public boolean isLong(@NotNull String path) {
        return config.isLong(path);
    }

    public @Nullable List<?> getList(@NotNull String path) {
        return config.getList(path);
    }

    @Contract("_, !null -> !null")
    public @Nullable List<?> getList(@NotNull String path, @Nullable List<?> def) {
        return config.getList(path, def);
    }

    public boolean isList(@NotNull String path) {
        return config.isList(path);
    }

    public @NotNull List<String> getStringList(@NotNull String path) {
        return config.getStringList(path);
    }

    public @NotNull List<Integer> getIntegerList(@NotNull String path) {
        return config.getIntegerList(path);
    }

    public @NotNull List<Boolean> getBooleanList(@NotNull String path) {
        return config.getBooleanList(path);
    }

    public @NotNull List<Double> getDoubleList(@NotNull String path) {
        return config.getDoubleList(path);
    }

    public @NotNull List<Float> getFloatList(@NotNull String path) {
        return config.getFloatList(path);
    }

    public @NotNull List<Long> getLongList(@NotNull String path) {
        return config.getLongList(path);
    }

    public @NotNull List<Byte> getByteList(@NotNull String path) {
        return config.getByteList(path);
    }

    public @NotNull List<Character> getCharacterList(@NotNull String path) {
        return config.getCharacterList(path);
    }

    public @NotNull List<Short> getShortList(@NotNull String path) {
        return config.getShortList(path);
    }

    public @NotNull List<Map<?, ?>> getMapList(@NotNull String path) {
        return config.getMapList(path);
    }

    public <T> @Nullable T getObject(@NotNull String path, @NotNull Class<T> clazz) {
        return config.getObject(path, clazz);
    }

    @Contract("_, _, !null -> !null")
    public <T> @Nullable T getObject(@NotNull String path, @NotNull Class<T> clazz, @Nullable T def) {
        return config.getObject(path, clazz, def);
    }

    public <T extends ConfigurationSerializable> @Nullable T getSerializable(@NotNull String path, @NotNull Class<T> clazz) {
        return config.getSerializable(path, clazz);
    }

    @Contract("_, _, !null -> !null")
    public <T extends ConfigurationSerializable> @Nullable T getSerializable(@NotNull String path, @NotNull Class<T> clazz, @Nullable T def) {
        return config.getSerializable(path, clazz, def);
    }

    public @Nullable Vector getVector(@NotNull String path) {
        return config.getVector(path);
    }

    @Contract("_, !null -> !null")
    public @Nullable Vector getVector(@NotNull String path, @Nullable Vector def) {
        return config.getVector(path, def);
    }

    public boolean isVector(@NotNull String path) {
        return config.isVector(path);
    }

    public @Nullable OfflinePlayer getOfflinePlayer(@NotNull String path) {
        return config.getOfflinePlayer(path);
    }

    @Contract("_, !null -> !null")
    public @Nullable OfflinePlayer getOfflinePlayer(@NotNull String path, @Nullable OfflinePlayer def) {
        return config.getOfflinePlayer(path, def);
    }

    public boolean isOfflinePlayer(@NotNull String path) {
        return config.isOfflinePlayer(path);
    }

    public @Nullable ItemStack getItemStack(@NotNull String path) {
        return config.getItemStack(path);
    }

    @Contract("_, !null -> !null")
    public @Nullable ItemStack getItemStack(@NotNull String path, @Nullable ItemStack def) {
        return config.getItemStack(path, def);
    }

    public boolean isItemStack(@NotNull String path) {
        return config.isItemStack(path);
    }

    public @Nullable Color getColor(@NotNull String path) {
        return config.getColor(path);
    }

    @Contract("_, !null -> !null")
    public @Nullable Color getColor(@NotNull String path, @Nullable Color def) {
        return config.getColor(path, def);
    }

    public boolean isColor(@NotNull String path) {
        return config.isColor(path);
    }

    public @Nullable Location getLocation(@NotNull String path) {
        return config.getLocation(path);
    }

    @Contract("_, !null -> !null")
    public @Nullable Location getLocation(@NotNull String path, @Nullable Location def) {
        return config.getLocation(path, def);
    }

    public boolean isLocation(@NotNull String path) {
        return config.isLocation(path);
    }

    @Nullable
    public ConfigurationSection getConfigurationSection(@NotNull String path) {
        return config.getConfigurationSection(path);
    }

    public boolean isConfigurationSection(@NotNull String path) {
        return config.isConfigurationSection(path);
    }

    @NotNull
    public static String createPath(@NotNull ConfigurationSection section, @Nullable String key) {
        return MemorySection.createPath(section, key);
    }

    @NotNull
    public static String createPath(@NotNull ConfigurationSection section, @Nullable String key, @Nullable ConfigurationSection relativeTo) {
        return MemorySection.createPath(section, key, relativeTo);
    }
}

