package ru.MrSinkaaa.TNTGames.shared.config.model.optional;

import java.util.Objects;

public class OptLong implements Opt {

    private boolean present;
    private long value;

    private OptLong() {}

    private OptLong(long value, boolean present) {
        this.value = value;
        this.present = present;
    }

    // getters

    @Override
    public boolean isPresent() {
        return present;
    }

    public long get() {
        return value;
    }

    public long get(long def) {
        if (!present) return def;
        else return value;
    }

    // equality

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OptLong that)) return false;
        return present == that.present && value == that.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(present, value);
    }

    // static

    public static OptLong of(long value) {
        return new OptLong(value, true);
    }

    public static OptLong empty() {
        return new OptLong();
    }

    public static OptLong empty(long def) {
        return new OptLong(def, false);
    }
}