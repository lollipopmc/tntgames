package ru.MrSinkaaa.TNTGames.shared.config;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;
import ua.i0xhex.lib.yaml.bukkit.file.YamlConfiguration;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

public abstract class AbstractConfig {

    protected JavaPlugin plugin;
    protected File file;

    protected String resource;
    protected char pathSeparator = '.';

    protected YamlConfiguration config;

    /**
     * Load config from file name. File chosen from default levelup directory.
     * Resource chosen from root directory in jar.
     *
     * @param plugin levelup
     * @param name file name (ex "config.yml")
     * @param useResource copy resource or not if file not exists
     */
    public AbstractConfig(JavaPlugin plugin, String name, boolean useResource) {
        this(plugin, name, useResource, '.');
    }

    /**
     * Load config from file name. File chosen from default levelup directory.
     * Resource chosen from root directory in jar.
     *
     * @param plugin levelup
     * @param name file name (ex "config.yml")
     * @param useResource copy resource or not if file not exists
     * @param pathSeparator path separator
     */
    public AbstractConfig(JavaPlugin plugin, String name, boolean useResource, char pathSeparator) {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), name);

        if (useResource) this.resource = name;
        this.pathSeparator = pathSeparator;
        setupNewConfiguration();
    }

    /**
     * Load config by file without using resource.
     *
     * @param plugin levelup
     * @param file file
     */
    public AbstractConfig(JavaPlugin plugin, File file) {
        this.plugin = plugin;
        this.file = file;
        setupNewConfiguration();
    }

    /**
     * Load config from file and resource.
     * Resource used if file not exists.
     *
     * @param plugin levelup
     * @param file file
     * @param resource resource (ex "config.yml")
     */
    public AbstractConfig(JavaPlugin plugin, File file, String resource) {
        this.plugin = plugin;
        this.file = file;
        this.resource = resource;
        setupNewConfiguration();
    }

    // load & save

    /**
     * Load config from file only.
     * If file not exists, then it will be created and empty config loaded.
     */
    public void loadConfig() {
        try {
            File dir = file.getParentFile();
            createDirIfNotExist(dir);
            createFileIfNotExist(file);

            setupNewConfiguration();

            config.load(file);
        } catch (Exception ex) {
            throw new IllegalStateException("Failed to load config: " + file.getName(), ex);
        }
    }

    /**
     * Load config with resource provided.
     * If file not exists then config from given resource will load.
     *
     * @param resource resource (ex. "config.yml")
     */
    public void loadConfig(String resource) {
        try {
            File dir = file.getParentFile();
            createDirIfNotExist(dir);
            createFileIfNotExist(file);

            setupNewConfiguration();

            InputStream stream = plugin.getClass().getResourceAsStream("/" + resource);
            InputStreamReader reader = new InputStreamReader(stream);

            YamlConfiguration defaults = new YamlConfiguration();
            defaults.options().pathSeparator(pathSeparator);
            defaults.load(reader);

            config.setDefaults(defaults);
            config.load(file);
        } catch (Exception ex) {
            throw new IllegalStateException("Failed to load config: " + file.getName(), ex);
        }
    }

    public YamlConfiguration getYamlConfiguration() {
        return config;
    }

    // protected

    protected void saveConfig() {
        try {
            File dir = file.getParentFile();
            createDirIfNotExist(dir);
            config.save(file);
        } catch (Exception ex) { ex.printStackTrace(); }
    }

    protected void load() {
        if (resource != null) loadConfig(resource);
        else loadConfig();
        saveConfig();

        // temp fix (defaults not set to loaded config from resource)
        loadConfig();
    }

    // private

    private void setupNewConfiguration() {
        config = new YamlConfiguration();
        config.options().copyDefaults(true);
        config.options().pathSeparator(pathSeparator);
    }

    private void createDirIfNotExist(File dir) {
        if (!dir.isDirectory() && !dir.mkdirs())
            throw new IllegalStateException("Failed to create directory " + dir.getName());
    }

    private void createFileIfNotExist(File file) {
        try {
            if (!file.isFile() && !file.createNewFile())
                throw new IllegalStateException("File creation returned false.");
        } catch (Exception ex) {
            throw new IllegalStateException("Failed to create file " + file.getName(), ex);
        }
    }

    // classes

    public static abstract class Section {

        protected ConfigurationSection section;

        public Section() {
            //
        }

        public Section(ConfigurationSection parent, String name) {
            section = parent.getConfigurationSection(name);
            if (section == null) section = parent.createSection(name);
        }
    }
}
