package ru.MrSinkaaa.TNTGames.shared.sideboard;

import me.clip.placeholderapi.PlaceholderAPI;
import net.kyori.adventure.text.serializer.legacy.LegacyComponentSerializer;
import org.bukkit.Bukkit;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class SideboardManager extends Manager {

    private Map<UUID, Sideboard> boards;

    private String currentMotd;
    private String currentDate;
    private String currentTime;

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy").withZone(ZoneId.systemDefault());
    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm").withZone(ZoneId.systemDefault());


    public SideboardManager(LollipopTNTGames plugin) {
        super(plugin);
        this.boards = new HashMap<>();
    }

    @Override
    public void onEnable() {
        //run date & motd updater
        currentMotd = "tr";
        currentDate = "dd/MM/yyyy";
        Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            currentMotd = LegacyComponentSerializer.legacySection().serialize(Bukkit.getServer().motd());
            var instant = Instant.now();
            currentDate = DATE_FORMATTER.format(instant);
            currentTime = TIME_FORMATTER.format(instant);
        }, 0L, 20L);
        new SideboardListener(this).register();
    }

    @Override
    public void onDisable() {
        removeBoard();
    }

    //func
    public void updateBoard(SideboardTemplate template, Replacer replacer) {
        for(GamePlayer player : plugin.getPlayers()) {
            updateBoard(template, replacer, player);
        }
    }

    public void updateBoard(SideboardTemplate template, Replacer replacer, GamePlayer player) {
        if(template == null) return;

        Sideboard sideboard = boards.computeIfAbsent(player.getUniqueId(), k ->
              new Sideboard(player.getHandle(), template.getTitle()));

        sideboard.updateTitle(template.getTitle());
        sideboard.updateLines(setPlaceholders(player, template.getBody(), replacer).split("\n"));
    }

    public void removeBoard() {
        for(GamePlayer player : plugin.getPlayers()) {
            removeBoard(player);
        }
    }

    public void removeBoard(GamePlayer player) {
        UUID uuid = player.getUniqueId();
        Sideboard sideboard = boards.remove(uuid);
        if(sideboard != null) sideboard.remove();
    }

    //private
    private String setPlaceholders(GamePlayer player, String text, Replacer replacer) {
        //motd & date
        StaticReplacer baseReplacer = new StaticReplacer();
        baseReplacer.set("motd", currentMotd);
        baseReplacer.set("date", currentDate);
        baseReplacer.set("time", currentTime);
        text = baseReplacer.apply(text);

        //custom
        if(replacer != null)
            text = replacer.apply(text, player);

        //papi
        if(Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI"))
            text = PlaceholderAPI.setPlaceholders(player, text);

        return text;
    }

}
