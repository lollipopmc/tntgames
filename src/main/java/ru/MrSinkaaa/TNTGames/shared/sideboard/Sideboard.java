package ru.MrSinkaaa.TNTGames.shared.sideboard;

import com.comphenix.protocol.wrappers.EnumWrappers;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import static ru.MrSinkaaa.TNTGames.common.packet.WrapperPlayServerScoreboardObjective.Mode.*;
import ru.MrSinkaaa.TNTGames.common.packet.WrapperPlayServerScoreboardDisplayObjective;
import ru.MrSinkaaa.TNTGames.common.packet.WrapperPlayServerScoreboardObjective;
import ru.MrSinkaaa.TNTGames.common.packet.WrapperPlayServerScoreboardScore;
import ru.MrSinkaaa.TNTGames.common.util.ExecutorUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Sideboard {

    private static final String ID = "tr";

    private Player player;

    private String title;
    private List<String> lines;

    /**
     * Create new sideboard for player
     *
     * @param player player
     * @param title sideboard title
     */
    public Sideboard(Player player, String title) {
        this.player = player;

        this.title = title;
        this.lines = new ArrayList<>();

        sendObjectivePacket(ADD_OBJECTIVE);
        sendDisplayObjectivePacket();
    }

    //getters

    /**
     * Get sideboard title
     *
     * @return title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Get sideboard lines
     *
     * @return lines
     */
    public List<String> getLines() {
        return lines;
    }

    //func

    /**
     * Update sideboard title
     *
     * @param title new title
     */
    public void updateTitle(String title) {
        if(this.title.equals(title)) return;
        this.title = title;

        sendObjectivePacket(UPDATE_VALUE);
    }

    /**
     * Update sideboard line
     *
     * @param index line index starting from 0
     * @param line new text
     */
    public void updateLine(int index, String line) {
        if(index < lines.size()) {
            lines.set(index, line);
//            sendScorePacket(getScoreByLine(index), SCORE_UPDATED);
        } else {
            List<String> newLines = new ArrayList<>(lines);

            //add empty lines before actual line if needed
            if(index > lines.size()) {
                for(int i = lines.size(); i < index; i++) {
                    newLines.add("");
                }
            }
            newLines.add(line);
            updateLines(newLines);
        }
    }

    /**
     * Remove sideboard line
     *
     * @param index line index starting from 0
     */
    public void removeLine(int index) {
        if(index >= lines.size()) return;

        List<String> lines = new ArrayList<>(this.lines);
        lines.remove(index);
        updateLines(lines);
    }

    /**
     * Update sideboard lines
     *
     * @param lines new lines
     */
    public void updateLines(String... lines) {
        updateLines(Arrays.asList(lines));
    }

    /**
     * Update sideboard lines
     *
     * @param lines new lines
     */
    public void updateLines(Collection<String> lines) {
        List<String> oldLines = new ArrayList<>(this.lines);
        this.lines.clear();
        this.lines.addAll(lines);

        int linesSize = this.lines.size();
        if(oldLines.size() != linesSize) {
            List<String> oldLinesCopy = new ArrayList<>(oldLines);
            if(oldLines.size() > linesSize) {
                for(int i = oldLinesCopy.size(); i > linesSize; i--) {
//                    sendScorePacket(i - 1, REMOVE);

                    oldLines.remove(0);
                }
            } else {
                for(int i = oldLinesCopy.size(); i < linesSize; i++) {
//                    sendScorePacket(i, CHANGE);

                    oldLines.add(oldLines.size() - i, getLineByScore(i));
                }
            }
        }
    }

    /**
     * Remove sideboard for player
     */
    public void remove() {
        sendObjectivePacket(REMOVE_OBJECTIVE);
    }

    //private
    private void sendObjectivePacket(int mode) {
        String title = this.title;

        ExecutorUtils.SIDEBOARD.submit(() -> {
            WrapperPlayServerScoreboardObjective packet = new WrapperPlayServerScoreboardObjective();
            packet.setName(ID);
            packet.setMode(mode);

            if(mode != REMOVE_OBJECTIVE)
                packet.setDisplayName(WrappedChatComponent.fromText(title));

            packet.sendPacket(player);
        });
    }

    private void sendDisplayObjectivePacket() {
        ExecutorUtils.SIDEBOARD.submit(() -> {
            WrapperPlayServerScoreboardDisplayObjective packet = new WrapperPlayServerScoreboardDisplayObjective();
            packet.setScoreName(ID);
            packet.setPosition(1);

            packet.sendPacket(player);
        });
    }

    private void sendScorePacket(int score, EnumWrappers.ScoreboardAction action) {
        ExecutorUtils.SIDEBOARD.submit(() -> {
            WrapperPlayServerScoreboardScore packet = new WrapperPlayServerScoreboardScore();
            packet.setObjectiveName(ID);
            packet.setScoreName(getColorCode(score));
            packet.setValue(score);
            packet.setScoreboardAction(action);

            packet.sendPacket(player);
        });
    }

    private String[] splitLine(String line) {
        String prefix;
        String suffix = null;

        if(line.length() <= 64) {
            prefix = line;
        } else {
            //set prefix
            int index = line.charAt(63) == '&' ? 63 : 64;
            prefix = line.substring(0, index);

            //set suffix
            String colors = ChatColor.getLastColors(prefix);
            String tmp = line.substring(index);
            ChatColor c = null;
            if(tmp.length() >= 2 && tmp.charAt(0) == '&')
                c = ChatColor.getByChar(tmp.charAt(1));

            boolean add = c == null || c.isFormat();
            suffix = (add ? (colors.isEmpty() ? "§r" : colors) : "") + tmp;
        }

        if(suffix != null && suffix.length() > 64)
            suffix = suffix.substring(0, 64);

        return new String[] {prefix, suffix};
    }

    private String getColorCode(int score) {
        return ChatColor.values()[score].toString();
    }

    private int getScoreByLine(int line) {
        return lines.size() - line - 1;
    }

    private String getLineByScore(int score) {
        return getLineByScore(lines,score);
    }

    private String getLineByScore(List<String> lines, int score) {
        return lines.get(lines.size() - score - 1);
    }
}
