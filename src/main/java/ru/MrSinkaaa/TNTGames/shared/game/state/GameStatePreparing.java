package ru.MrSinkaaa.TNTGames.shared.game.state;

import net.kyori.adventure.title.Title;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitTask;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.debug.DebugType;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.config.configs.Config;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.GameState;
import ru.MrSinkaaa.TNTGames.shared.game.state.listener.preparing.StatePlayerListener;
import ru.MrSinkaaa.TNTGames.shared.game.state.listener.preparing.StateWorldListener;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardManager;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardTemplate;
import ru.MrSinkaaa.TNTGames.shared.world.WorldManager;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static ru.MrSinkaaa.TNTGames.LollipopTNTGames.debugInfo;

public class GameStatePreparing extends GameState {

    private SideboardTemplate sideboardTemplate;
    private List<BukkitListener> listeners = List.of(
          new StatePlayerListener(this),
          new StateWorldListener(this));

    private BukkitTask titleWaitingTask;

    public GameStatePreparing(Game game) {
        super(game);

        //cache sideboard template
        Lang lang = plugin.lang();
        sideboardTemplate = new SideboardTemplate(lang.msgRaw("misc.sideboard.preparing.template"));
    }

    //func
    @Override
    public void start() {
        debugInfo(DebugType.STATE, "preparing (start): '%s' %d", game.getSettings().getName(), Game.ID);

        super.start();

        //update board
        updateSideboard();

        //teleport players to limbo
        teleportPlayersToLimbo();
        startWaitingTitleTask();

        //unload world
        WorldManager worldManager = plugin.getManager(WorldManager.class);
        worldManager.unload(game.getWorld());

        //start next game
        GameManager gameManager = plugin.getManager(GameManager.class);
        Game game = gameManager.startNextGame();

        //self stop
        stop();

        //invalidate previous game
        this.game.invalidate();
        this.game = null;
    }

    @Override
    public void stop() {
        debugInfo(DebugType.STATE, "preparing (stop): '%s' %d", game.getSettings().getName(), Game.ID);

        super.stop();
        stopWaitingTitleTask();
    }

    //private
    @Override
    protected List<BukkitListener> getListeners() {
        return listeners;
    }

    private void teleportPlayersToLimbo() {
        Config config = plugin.config();
        Location locationToTeleport = config.limbo.toLocation();

        for(GamePlayer player : game.getPlayers()) {
            player.setGameMode(GameMode.SPECTATOR);
            player.teleport(locationToTeleport);
        }
    }

    private void startWaitingTitleTask() {
        Lang lang = plugin.lang();
        Set<GamePlayer> players = new HashSet<>(game.getPlayers());

        titleWaitingTask = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, () -> {
            Title title = lang.title("title.notify.preparing.preparing").toTitle();
            for (GamePlayer player : players) {
                player.showTitle(title);
            }
        }, 0L,20L );
    }

    private void stopWaitingTitleTask() {
        if(titleWaitingTask != null) {
            titleWaitingTask.cancel();
            titleWaitingTask = null;
        }
    }

    private void updateSideboard() {
        Lang lang = plugin.lang();

        SideboardManager sideboardManager = plugin.getManager(SideboardManager.class);
        sideboardManager.updateBoard(sideboardTemplate, null);
    }
}
