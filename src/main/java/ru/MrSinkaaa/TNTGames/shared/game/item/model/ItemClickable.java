package ru.MrSinkaaa.TNTGames.shared.game.item.model;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.scheduler.BukkitRunnable;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.item.ItemInfo;
import ru.MrSinkaaa.TNTGames.shared.item.ItemManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;

public class ItemClickable implements Listener {

    protected LollipopTNTGames plugin;

    protected String id;
    protected ItemInfo info;
    protected Map<Integer, BukkitRunnable> tasks = new ConcurrentHashMap<>();
    protected int tick;

    public ItemClickable(String id) {
        this.plugin = LollipopTNTGames.getInstance();

        this.id = id;
        this.info = plugin.getManager(ItemManager.class).getItemOrFail(id);
        registerListener();
    }

    public ItemClickable(ItemInfo info) {
        this.plugin = LollipopTNTGames.getInstance();

        this.id = info.getId();
        this.info = info;
        registerListener();
    }

    //getters
    public String getId() {
        return id;
    }

    public ItemInfo getInfo() {
        return info;
    }

    //func
    public ItemStack createItem() {
        return info.getTemplate().getStaticItem();
    }

    public ItemStack createItem(Function<String, String> replacement) {
        return info.getTemplate().getStaticItem(replacement);
    }

    public void tick() {
        tick++;
    }

    public void cancel() {
        //cancel all tasks
        List<BukkitRunnable> taskList = new ArrayList<>(tasks.values());
        taskList.forEach(BukkitRunnable::cancel);
    }

    public void registerTask(BukkitRunnable runnable) {
        tasks.put(runnable.getTaskId(), runnable);
    }

    public void unregisterTask(BukkitRunnable runnable) {
        tasks.remove(runnable.getTaskId());
    }

    //func : events
    public void acceptEvent(PlayerInteractEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());
        switch (e.getAction()) {
            case LEFT_CLICK_BLOCK:
                onLeftClick(e, player, e.getClickedBlock());
                break;
            case RIGHT_CLICK_BLOCK:
                onRightClick(e, player, e.getClickedBlock());
                break;
            case LEFT_CLICK_AIR:
                onLeftClick(e, player, null);
                break;
            case RIGHT_CLICK_AIR:
                onRightClick(e, player, null);
                break;
            case PHYSICAL:
                e.setCancelled(true);
                break;
        }
    }

    public void acceptEvent(PlayerDropItemEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());
        onPlayerDropItem(e, player);
    }

    public void acceptEvent(BlockPlaceEvent e) {
        GamePlayer player = plugin.getPlayer(e.getPlayer());
        onBlockPlace(e, player, e.getBlockPlaced());
    }

    //protected
    protected void onLeftClick(PlayerInteractEvent e, GamePlayer player, Block block) {

    }

    protected void onRightClick(PlayerInteractEvent e, GamePlayer player, Block block) {

    }

    protected void onPlayerDropItem(PlayerDropItemEvent e, GamePlayer player) {
        e.setCancelled(true);
    }

    protected void onBlockPlace(BlockPlaceEvent e, GamePlayer player, Block block) {
        e.setCancelled(true);
    }

    protected void consumeItem(GamePlayer player, EquipmentSlot slot) {
        PlayerInventory inventory = player.getInventory();
        ItemStack item = inventory.getItem(slot);

        //filter (no item in slot)
        if(item == null) return;

        //do
        if(item.getAmount() > 1) {
            item.setAmount(item.getAmount() - 1);
            inventory.setItem(slot, item);
        } else inventory.setItem(slot, null);
    }

    protected void consumeItem(GamePlayer player, int slot) {
        PlayerInventory inventory = player.getInventory();
        ItemStack item = inventory.getItem(slot);

        //filter (no item in slot)
        if(item == null) return;

        //do
        if(item.getAmount() > 1) {
            item.setAmount(item.getAmount() - 1);
            inventory.setItem(slot, item);
        }
    }

    //private
    private void registerListener() {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }
}
