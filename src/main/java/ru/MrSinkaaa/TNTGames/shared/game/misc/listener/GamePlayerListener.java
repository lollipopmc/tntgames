package ru.MrSinkaaa.TNTGames.shared.game.misc.listener;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.*;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.Permission;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateWaiting;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class GamePlayerListener extends BukkitListener {

    private Map<String, Map<Class<?>, Object>> loadedDataMap = new ConcurrentHashMap<>();

    public GamePlayerListener(LollipopTNTGames plugin) {
        super(plugin);
    }

    //events : join & quit
    @EventHandler(priority = EventPriority.MONITOR)
    public void onAsyncPlayerPreLogin_Monitor(AsyncPlayerPreLoginEvent e) {
        //load data
        String name = e.getName();
        if(e.getLoginResult() == AsyncPlayerPreLoginEvent.Result.ALLOWED)
            loadCustomData(name);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerLogin_Low(PlayerLoginEvent e) {
        //filter : not allowed before
        if(e.getResult() != PlayerLoginEvent.Result.ALLOWED) return;

        //check : not allowed to join
        checkPlayerAllowedToJoin(e);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerLogin_Monitor(PlayerLoginEvent e) {
        //unload data if login disallowed
        String name = e.getPlayer().getName();
        if(e.getResult() != PlayerLoginEvent.Result.ALLOWED)
            unloadCustomData(name);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerJoin(PlayerJoinEvent e) {
        //check : not allowed to join
        checkPlayerAllowedToJoin(e);

        //register player
        GameManager gameManager = plugin.getManager(GameManager.class);
        GamePlayer player = gameManager.registerPlayer(e.getPlayer());

        //detect permission group
        var playerGroup = plugin.getPermission().getPrimaryGroup(player.getHandle());
        if(playerGroup == null || playerGroup.isEmpty())
            playerGroup = "default";
        player.setPermissionGroup(playerGroup);

        //set data
        String name = player.getName();
        player.setCustomDataMap(loadedDataMap.remove(name));

        e.joinMessage(null);
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit_Monitor(PlayerQuitEvent e) {
        //unregister player & unload data
        GameManager gameManager = plugin.getManager(GameManager.class);
        gameManager.unregisterPlayer(e.getPlayer());
        unloadCustomData(e.getPlayer().getName());

        //misc
        e.quitMessage(null);
    }

    //private
    private boolean checkPlayerAllowedToJoin(PlayerEvent e) {
        Lang lang = plugin.lang();
        Player player = e.getPlayer();
        GameManager gameManager = plugin.getManager(GameManager.class);
        Game game = gameManager.getCurrentGame();

        //check : game not running
        if(!gameManager.isGameStarted()) {
            if(!player.hasPermission(Permission.BYPASS_STOPPED)) {
                kickPlayerFromEvent(e, lang.msg("misc.kick.stopped"));
                return false;
            }
            return true;
        }

        //check : game is not in waiting state
        if(game != null && !(game.getState() instanceof GameStateWaiting)) {
            kickPlayerFromEvent(e, lang.msg("misc.kick.running"));
            return false;
        }

        //check : full
        int online = gameManager.getPlayers().size();
        int max = game.getSettings().getMaxPlayers();

        if(online >= max) {
            kickPlayerFromEvent(e, lang.msg("misc.kick.full"));
            return false;
        }
        return true;
    }

    private void kickPlayerFromEvent(PlayerEvent e, Component reason) {
        if(e instanceof PlayerJoinEvent) {
            Bukkit.getScheduler().runTask(plugin, () -> {
                Player player = e.getPlayer();
                player.kick(reason);
            });
        } else if(e instanceof PlayerLoginEvent) {
            PlayerLoginEvent playerLoginEvent = (PlayerLoginEvent) e;
            playerLoginEvent.setResult(PlayerLoginEvent.Result.KICK_OTHER);
            playerLoginEvent.kickMessage(reason);
        }
    }

    protected void loadCustomData(String name) {
        Map<Class<?>, Object> map = new HashMap<>();
        GamePlayer.getDataLoaders().forEach(loader -> loader.accept(name, map));
        loadedDataMap.put(name, map);
    }

    protected void unloadCustomData(String name) {
        loadedDataMap.remove(name);
    }

}
