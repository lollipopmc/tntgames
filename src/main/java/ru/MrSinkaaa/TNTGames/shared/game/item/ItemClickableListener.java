package ru.MrSinkaaa.TNTGames.shared.game.item;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.util.ItemUtils;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickable;

public class ItemClickableListener extends BukkitListener {

    private ItemClickableManager manager;

    public ItemClickableListener(ItemClickableManager manager) {
        super(manager.getPlugin());
        this.manager = manager;
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onPlayerInteract(PlayerInteractEvent e) {
        ItemClickable clickable = getClickable(e.getItem());
        if (clickable != null)
            clickable.acceptEvent(e);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onPlayerDropItem(PlayerDropItemEvent e) {
        ItemClickable clickable = getClickable(e.getItemDrop().getItemStack());
        if (clickable != null)
            clickable.acceptEvent(e);
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent e) {
        ItemClickable clickable = getClickable(e.getItemInHand());
        if (clickable != null)
            clickable.acceptEvent(e);
    }

    // private

    private ItemClickable getClickable(ItemStack item) {
        // filter : game not running
        GameManager gameManager = plugin.getManager(GameManager.class);
        if (gameManager.getCurrentGame() == null) return null;

        // filter : no item clicked
        if (item == null) return null;

        // filter : no id set
        String id = ItemUtils.getId(item);
        if (id == null) return null;

        // do
        return manager.getItem(id);
    }
}

