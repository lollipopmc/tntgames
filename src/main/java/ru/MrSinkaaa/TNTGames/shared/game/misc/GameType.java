package ru.MrSinkaaa.TNTGames.shared.game.misc;

import java.util.Locale;

public enum GameType {

    COMMON, PVP, BOW;

    public static GameType get(String name) {
        try {
            return valueOf(name.toUpperCase(Locale.ENGLISH));
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
