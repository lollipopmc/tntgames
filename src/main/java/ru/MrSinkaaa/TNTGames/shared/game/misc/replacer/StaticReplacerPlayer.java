package ru.MrSinkaaa.TNTGames.shared.game.misc.replacer;

import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;

public class StaticReplacerPlayer extends StaticReplacer {

    public StaticReplacerPlayer(GamePlayer player) {
        this(player, "");
    }

    public StaticReplacerPlayer(GamePlayer player, String prefix) {
        if(!prefix.isEmpty()) prefix += "_";

        //prepare
        String playerName = player.getName();

        //add replacements
        set(prefix + "player_name", playerName);
    }
}
