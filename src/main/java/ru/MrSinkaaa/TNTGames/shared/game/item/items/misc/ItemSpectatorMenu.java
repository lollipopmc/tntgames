package ru.MrSinkaaa.TNTGames.shared.game.item.items.misc;

import org.bukkit.block.Block;
import org.bukkit.event.player.PlayerInteractEvent;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickable;
import ru.MrSinkaaa.TNTGames.shared.menu.menus.MenuSpectatePlayers;

public class ItemSpectatorMenu extends ItemClickable {

    public ItemSpectatorMenu() {
        super("spectator_menu");
    }

    @Override
    protected void onRightClick(PlayerInteractEvent e, GamePlayer player, Block block) {
        super.onRightClick(e, player, block);
        new MenuSpectatePlayers(player).open();
    }
}
