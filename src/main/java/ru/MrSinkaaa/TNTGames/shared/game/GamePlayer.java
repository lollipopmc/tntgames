package ru.MrSinkaaa.TNTGames.shared.game;

import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.common.model.WrappedPlayer;
import ru.MrSinkaaa.TNTGames.shared.game.misc.SpectatorSpeed;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public class GamePlayer extends WrappedPlayer {

    private String permissionGroup;

    private Game game;

    private boolean spectator;
    private int points;
    private SpectatorSpeed spectatorSpeed;
    private Map<Class<?>, Object> customDataMap;

    private boolean quitWhenAlive;
    private int kills;
    private int deaths;

    public GamePlayer(Player handle) {
        super(handle);
        this.spectatorSpeed = SpectatorSpeed.X1;
        this.customDataMap = new HashMap<>();
    }

    //getters
    public String getPermissionGroup() {
        return permissionGroup;
    }

    public Game getGame() {
        return game;
    }

    public boolean isSpectator() {
        return spectator;
    }

    public int getPoints() {
        return points;
    }

    public int getKills() {
        return kills;
    }

    public int getDeaths() {
        return deaths;
    }

    public boolean isQuitWhenAlive() {
        return quitWhenAlive;
    }

    public SpectatorSpeed getSpectatorSpeed() {
        return spectatorSpeed;
    }

    public Map<Class<?>, Object> getCustomDataMap() {
        return customDataMap;
    }

    //setters
    public void setPermissionGroup(String permissionGroup) {
       this.permissionGroup = permissionGroup;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setSpectator(boolean spectator) {
        this.spectator = spectator;
    }

    public void setSpectatorSpeed(SpectatorSpeed spectatorSpeed) {
        this.spectatorSpeed = spectatorSpeed;
        this.handle.setFlySpeed(0.1F * (float) spectatorSpeed.getCoef());
    }

    public void setCustomDataMap(Map<Class<?>, Object> customDataMap) {
        this.customDataMap = customDataMap;
    }

    public void setQuitWhenAlive(boolean quitWhenAlive) {
        this.quitWhenAlive = quitWhenAlive;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }

    // modifiers

    public void incrementKills() {
        kills++;
    }

    public void incrementDeaths() {
        deaths++;
    }

    //func
    public void reset() {
        spectator = false;
        quitWhenAlive = false;
    }

    //static
    private static Set<BiConsumer<String, Map<Class<?>, Object>>> DATA_LOADERS = new HashSet<>();

    public static void registerDataLoader(BiConsumer<String, Map<Class<?>, Object>> loader) {
        DATA_LOADERS.add(loader);
    }

    public static void unregisterDataLoader(BiConsumer<String, Map<Class<?>, Object>> loader) {
        DATA_LOADERS.remove(loader);
    }

    public static Set<BiConsumer<String, Map<Class<?>, Object>>> getDataLoaders() {
        return DATA_LOADERS;
    }
}
