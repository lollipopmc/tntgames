package ru.MrSinkaaa.TNTGames.shared.game;


import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.common.model.Position;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.shared.game.misc.GameType;
import ru.MrSinkaaa.TNTGames.shared.game.misc.listener.GamePlayerListener;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameSettings;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameSettingsValidation;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateRunning;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateStopped;
import ru.MrSinkaaa.TNTGames.shared.world.WorldManager;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

public class GameManager extends Manager {

    //game
    private Game game;
    private boolean sleeping;

    //misc
    private Map<UUID, GamePlayer> players = new HashMap<>();

    private Map<String, GameSettings> activeGameSettings = new HashMap<>();
    private Map<String, GameSettings> temporaryGameSettings = new HashMap<>();

    public GameManager(LollipopTNTGames plugin) {
        super(plugin);
    }

    @Override
    public void onEnable() {
        loadGameSettings();
        prepareLimboWorld();

        //register listeners
        new GamePlayerListener(plugin).register();

        //start next game
        if(plugin.config().startRandomGame) {
            boolean syncSupported = Bukkit.getPluginManager().isPluginEnabled("LollipopSyncClient");
            if(!syncSupported) {
                startNextGame();
            } else {
                sleepCurrentGame();
            }
        }
    }

    //getters
    public Game getCurrentGame() {
        return game;
    }

    public GamePlayer getPlayer(UUID uuid) {
        return players.get(uuid);
    }

    public GamePlayer getPlayer(Player p) {
        return getPlayer(p.getUniqueId());
    }

    public Map<UUID, GamePlayer> getPlayers() {
        return Collections.unmodifiableMap(players);
    }

    public Map<String, GameSettings> getActiveGameSettings() {
        return Collections.unmodifiableMap(activeGameSettings);
    }

    public Map<String, GameSettings> getTemporaryGameSettings() {
        return Collections.unmodifiableMap(temporaryGameSettings);
    }

    //func
    public boolean isGameStarted() {
        return game != null && !(game.getState() instanceof GameStateStopped);
    }

    public boolean isGameRunning() {
        return game != null && (game.getState() instanceof GameStateRunning);
    }

    public boolean isGameSleeping() {
        return sleeping;
    }


    public Game startNextGame() {
        return startNextGame(game == null ? null : game.getSettings().getType());
    }

    public Game startNextGame(GameType type) {
        List<GameSettings> games;
        if(type != null) {
            games = activeGameSettings.values().stream()
                  .filter(s -> s.getType() == type)
                  .collect(Collectors.toCollection(ArrayList::new));
        } else {
            games = new ArrayList<>(activeGameSettings.values());
        }
        return startNextGame(games);
    }

    public void sleepCurrentGame() {
        sleeping = true;
        stopCurrentGame();
    }

    public void stopCurrentGame() {
        if(game == null) return;

        //stop game
        game.stop();
        GameStateStopped state = (GameStateStopped) game.getState();
        state.stop();

        //unload world
        WorldManager worldManager = plugin.getManager(WorldManager.class);
        worldManager.unload(game.getWorld());

        //nullify
        game = null;
    }

    //func : settings
    public GameSettings createGameSettingsEdit(String gameId, String worldName) {
        GameSettings settings = GameSettings.create(gameId,worldName);
        temporaryGameSettings.put(gameId,settings);
        return settings;
    }

    public GameSettings editGameSettings(String gameId) {
        GameSettings settings = temporaryGameSettings.get(gameId);
        if(settings == null) {
            settings = activeGameSettings.get(gameId);
            if(settings != null) {
                settings = GameSettings.copy(settings);
                temporaryGameSettings.put(gameId,settings);
                return settings;
            } else return null;
        } else return settings;
    }

    public void removeGameSettings(String gameId) {
        activeGameSettings.remove(gameId);
        temporaryGameSettings.remove(gameId);
    }

    public void saveGameSettingsEdit(String gameId) {
        GameSettings settings = temporaryGameSettings.get(gameId);
        if(settings != null) {
            activeGameSettings.put(gameId, settings);
            cancelGameSettingsEdit(gameId);
        }
    }

    public void cancelGameSettingsEdit(String gameId) {
        temporaryGameSettings.remove(gameId);
    }

    //func : uncommon
    public GamePlayer registerPlayer(Player p) {
        UUID uuid = p.getUniqueId();
        GamePlayer player = new GamePlayer(p);
        players.put(uuid, player);
        return player;
    }

    public GamePlayer unregisterPlayer(Player player) {
        UUID uuid = player.getUniqueId();
        return players.remove(uuid);
    }

    //private
    private Game startNextGame(List<GameSettings> games) {
        if(games.isEmpty()) {
            MiscUtils.warning("No game candidates found! Game start aborted.");
            return null;
        }

        //get random game
        Random random = new Random();
        GameSettings settings = games.get(random.nextInt(games.size()));

        //prepare world
        WorldManager worldManager = plugin.getManager(WorldManager.class);
        String worldName = settings.getWorldName();
        World world = worldManager.load(worldName);
        if(world == null) {
            MiscUtils.warning("World '%s' failed to load! Game start aborted.", worldName);
            return null;
        }

        world.setTime(6000);

        //prepare & start game
        sleeping = false;
        game = new Game(plugin,settings);
        game.start();

        //update server status
        Bukkit.setMaxPlayers(settings.getMaxPlayers());

        return game;
    }

    private void loadGameSettings() {
        File dir = new File(plugin.getDataFolder(), "games");
        if(!dir.isDirectory())
            dir.mkdirs();

        for(File file : dir.listFiles()) {
            //load from file
            String id = file.getName();
            id = id.substring(0, id.lastIndexOf('.'));
            GameSettings settings = GameSettings.load(id);

            //validate
            GameSettingsValidation validation = settings.validate();
            if(!validation.isPassed()) {
                MiscUtils.warning("Game '%s' failed validation and not loaded to pool.", id);
                continue;
            }

            //add
            activeGameSettings.put(id, settings);
        }
    }

    private void prepareLimboWorld() {
        Position position = plugin.config().limbo;
        String worldName = position.getWorldName();

        WorldManager worldManager = plugin.getManager(WorldManager.class);
        worldManager.load(worldName);
    }

}
