package ru.MrSinkaaa.TNTGames.shared.game.state.listener.running;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.world.StructureGrowEvent;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.shared.game.state.GameStateRunning;

public class StateWorldListener extends BukkitListener {

    private GameStateRunning state;

    public StateWorldListener(GameStateRunning state) {
        super(state.getPlugin());
        this.state = state;
    }

    //event : entities

    @EventHandler
    public void onEntityExplode(EntityExplodeEvent e) {
        e.setYield(0);
//        filterBlocksToExplode(e.blockList());
    }

    //event : blocks

    @EventHandler
    public void onBlockExplode(BlockExplodeEvent e) {
        e.setYield(0);
//        filterBlocksToExplode(e.blockList());
    }

    @EventHandler
    public void onLeavesDecay(LeavesDecayEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockGrow(BlockGrowEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockForm(BlockFormEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onBlockSpread(BlockSpreadEvent e) {
        if (e.getNewState().getType() == Material.FIRE)
            e.setCancelled(true);
    }

    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent e) {
        if (e.getCause() == BlockIgniteEvent.IgniteCause.SPREAD)
            e.setCancelled(true);
    }

    @EventHandler
    public void onEntityChangeBlock(EntityChangeBlockEvent e) {
            e.setCancelled(true);
    }

    @EventHandler
    public void onStructureGrow(StructureGrowEvent e) {
        e.setCancelled(true);
    }


}
