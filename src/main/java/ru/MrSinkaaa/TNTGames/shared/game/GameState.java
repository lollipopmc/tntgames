package ru.MrSinkaaa.TNTGames.shared.game;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.shared.event.GameStateStartEvent;
import ru.MrSinkaaa.TNTGames.shared.event.GameStateStopEvent;
import ru.MrSinkaaa.TNTGames.shared.game.item.ItemClickableManager;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickable;

import java.util.List;

public abstract class GameState {

    //base
    protected LollipopTNTGames plugin;
    protected GameManager manager;
    protected Game game;

    //runtime

    protected int tick;
    protected BukkitTask tickTask;

    public GameState(Game game) {
        this.plugin = LollipopTNTGames.getInstance();
        this.game = game;
        this.manager = plugin.getManager(GameManager.class);
    }

    public LollipopTNTGames getPlugin() {
        return plugin;
    }

    public Game getGame() {
        return game;
    }

    public GameManager getManager() {
        return manager;
    }

    public void start() {
        Bukkit.getPluginManager().callEvent(new GameStateStartEvent(this));
        registerListeners(getListeners());
        tickTask = Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            tick++;
            tick();
        }, 1L, 1L);
    }

    public void stop() {
        Bukkit.getPluginManager().callEvent(new GameStateStopEvent(this));
        ItemClickableManager itemClickableManager = plugin.getManager(ItemClickableManager.class);
        itemClickableManager.getItems().values().forEach(ItemClickable::cancel);

        unregisterListeners(getListeners());
        tickTask.cancel();
    }

    protected void tick() {}

    protected abstract List<BukkitListener> getListeners();

    private void registerListeners(List<BukkitListener> listeners) {
        for(BukkitListener listener : getListeners())
            listener.register();
    }

    private void unregisterListeners(List<BukkitListener> listeners) {
        for(BukkitListener listener : getListeners())
            listener.unreginter();
    }
}
