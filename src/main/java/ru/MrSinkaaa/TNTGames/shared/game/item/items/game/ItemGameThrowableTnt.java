package ru.MrSinkaaa.TNTGames.shared.game.item.items.game;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickable;

public class ItemGameThrowableTnt extends ItemClickable {

    // properties

    private double damageModifier;

    public ItemGameThrowableTnt() {
        super("game_throwable_tnt");

        ConfigurationSection properties = getInfo().getProperties();
        damageModifier = properties.getDouble("damage-modifier", 1.0D);
    }

    @Override
    protected void onRightClick(PlayerInteractEvent e, GamePlayer player, Block block) {
        e.setCancelled(true);
    }

    @Override
    protected void onLeftClick(PlayerInteractEvent e, GamePlayer player, Block block) {
        e.setCancelled(true);
        consumeItem(player, e.getHand());

        Location location = player.getEyeLocation();
        Vector direction = player.getLocation().getDirection();
        TNTPrimed primed = (TNTPrimed) player.getWorld().spawn(location, TNTPrimed.class);
        MiscUtils.setMetadata(primed, id, true);
        primed.setVelocity(direction.normalize().multiply(0.75));
    }

    // event : direct

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
        // filter : not entity of that item
        Entity damager = e.getDamager();
        if (!(damager instanceof TNTPrimed) || !MiscUtils.hasMetadata(damager, id)) {
            return;
        }

        // do
        e.setDamage(e.getDamage() * damageModifier);
    }
}
