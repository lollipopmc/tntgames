package ru.MrSinkaaa.TNTGames.shared.game.item;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.shared.game.item.items.misc.ItemSpectatorMenu;
import ru.MrSinkaaa.TNTGames.shared.game.item.items.misc.ItemSpectatorSpeed;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickable;
import ru.MrSinkaaa.TNTGames.shared.game.item.model.ItemClickableAction;
import ru.MrSinkaaa.TNTGames.shared.item.ItemManager;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ItemClickableManager extends Manager {

    private Map<String, ItemClickable> items = new HashMap<>();

    public ItemClickableManager(LollipopTNTGames plugin) {
        super(plugin);
    }

    @Override
    public void onEnable() {
        registerClickableItems();
        runItemClickableTicker();
        new ItemClickableListener(this).register();
    }

    public ItemClickable getItem(String id) {
        return items.get(id);
    }

    public Map<String, ItemClickable> getItems() {
        return Collections.unmodifiableMap(items);
    }

    //private
    private void registerClickableItems() {
        //custom actions
        ItemManager itemManager = plugin.getManager(ItemManager.class);
        itemManager.getItems().values().forEach(info -> {
            ConfigurationSection properties = info.getProperties();
           if(properties != null && properties.contains("custom-action"))
               registerItemClickable(new ItemClickableAction(info));
        });

        //base
        registerItemClickable(new ItemSpectatorMenu());
        registerItemClickable(new ItemSpectatorSpeed());
    }

    private void registerItemClickable(ItemClickable item) {
        items.put(item.getId(), item);
    }

    private void runItemClickableTicker() {
        Bukkit.getScheduler().runTaskTimer(plugin, () -> {
            items.values().forEach(ItemClickable::tick);
        }, 1L, 1L);
    }
}
