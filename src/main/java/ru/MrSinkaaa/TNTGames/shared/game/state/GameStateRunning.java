package ru.MrSinkaaa.TNTGames.shared.game.state;

import net.kyori.adventure.text.Component;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.block.Block;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitRunnable;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.BukkitListener;
import ru.MrSinkaaa.TNTGames.common.debug.DebugType;
import ru.MrSinkaaa.TNTGames.common.model.BlockPosition;
import ru.MrSinkaaa.TNTGames.common.model.Pos;
import ru.MrSinkaaa.TNTGames.common.model.Position;
import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.CombinedReplacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.DynamicReplacer;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.common.util.InvUtils;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.config.configs.Config;
import ru.MrSinkaaa.TNTGames.shared.game.Game;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.game.GameState;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameSettings;
import ru.MrSinkaaa.TNTGames.shared.game.state.listener.running.StatePlayerListener;
import ru.MrSinkaaa.TNTGames.shared.game.state.listener.running.StateWorldListener;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardManager;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardTemplate;

import java.time.Duration;
import java.util.List;
import java.util.Random;

import static ru.MrSinkaaa.TNTGames.LollipopTNTGames.debugInfo;

public class GameStateRunning extends GameState {

    private SideboardTemplate sideboardTemplate;
    private List<BukkitListener> listeners = List.of(
          new StatePlayerListener(this),
          new StateWorldListener(this));

    private int secondsToTheEnd;

    public GameStateRunning(Game game) {
        super(game);

        //cache sideboard template
        Lang lang = plugin.lang();
        sideboardTemplate = new SideboardTemplate(lang.msgRaw("misc.sideboard.running.template"));

        //prepare misc props
        Config config = plugin.config();
        secondsToTheEnd = config.minutesPerGame * 60;
    }

    @Override
    public void start() {
        debugInfo(DebugType.STATE, "running (start): '%s' %d", game.getSettings().getName(), Game.ID);

        super.start();
        spawnPlayers();

        Bukkit.getScheduler().runTaskTimer(LollipopTNTGames.getInstance(),() -> {
            getGame().players().forEach(p -> {
                Block b = getBlockUnderPlayer(p.getLocation().getBlockY() - 1, p.getLocation());

                if(b == null)
                    return;

                Block tnt = b.getLocation().clone().subtract(0,1,0).getBlock();
                if(tnt == null || tnt.getType() != Material.TNT) {
                    return;
                }

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        b.setType(Material.AIR);
                        b.getLocation().subtract(0,1,0).getBlock().setType(Material.AIR);
                    }
                }.runTaskLater(LollipopTNTGames.getInstance(),5L);
            });
        }, 10L, 4L);

        //update sideboard
        updateSideboard();
    }

    @Override
    public void stop() {
        debugInfo(DebugType.STATE, "running (stop): '%s' %d", game.getSettings().getName(), Game.ID);

        super.stop();
    }

    //func
    public void resetPlayerState(GamePlayer player) {
        InvUtils.clearInventory(player);
        player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(20);
        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
        player.setFoodLevel(20);
        player.setSaturation(5.0F);
        player.setAllowFlight(false);
        player.setFlying(false);
        player.setFireTicks(0);

        for(PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
    }

    public void broadcastToAll(Component message) {
        for(GamePlayer player : game.getPlayers()) {
            player.sendMessage(message);
        }
    }

    public void processPlayerLeaveGame(GamePlayer player) {
        int playersAlive = 0;
        GamePlayer lastPlayer = null;

        for(GamePlayer p : game.getPlayers()) {
            if(!p.isSpectator()) {
                playersAlive++;
                if(lastPlayer == null)
                    lastPlayer = p;
            }
        }

        if(playersAlive < 2) {
            finishGameWin(lastPlayer);
        }
    }

    @Override
    protected void tick() {
        super.tick();

        //tick break block

        //tick give points
        if(tick % 20 == 0) {
            secondsToTheEnd--;
            updateSideboard();

            if(secondsToTheEnd <= 0)
                finishGameTimeout();
        }

    }

    @Override
    protected List<BukkitListener> getListeners() {
        return listeners;
    }

    private void updateSideboard() {
        Lang lang = plugin.lang();

        String timeLeftFormatted = getTimeLeftFormatted();
        String map = game.getSettings().getName();

        Replacer replacer = new CombinedReplacer(
              new StaticReplacer()
              .set("time_left", timeLeftFormatted)
              .set("map", map),
              new DynamicReplacer<GamePlayer>()
                    .set("points", GamePlayer::getPoints));

        SideboardManager sideboardManager = plugin.getManager(SideboardManager.class);
        sideboardManager.updateBoard(sideboardTemplate,replacer);
    }

    private String getTimeLeftFormatted() {
        Duration duration = Duration.ofSeconds(secondsToTheEnd);
        int minutes = (int) duration.toMinutes();
        int seconds = duration.toSecondsPart();
        return String.format("%02d:%02d", minutes, seconds);
    }

    private void spawnPlayers() {
        GameSettings settings = game.getSettings();
        List<Position> locations = settings.getSpawnSettings().getSpawns();

        Random random = new Random();
        getGame().getPlayers().forEach(player -> {
            resetPlayerState(player);
        try {
            int randInt = random.nextInt(locations.size());
            player.teleport(locations.get(randInt).toLocation());
        } catch (Exception ex) {
            player.teleport(locations.get(0).toLocation());
        }});

    }

    private Block getBlockUnderPlayer(int y, Location location) {
        double add = 0.3D;
        Pos loc = new Pos(location.getX(), y, location.getZ());
        Block bEast = loc.getBlock(location.getWorld(), +add, -add);
        if (bEast.getType() != Material.AIR) {
            return bEast;
        }
        Block bSouth = loc.getBlock(location.getWorld(), -add, +add);
        if (bSouth.getType() != Material.AIR) {
            return bSouth;
        }
        Block bNorth = loc.getBlock(location.getWorld(), +add, +add);
        if (bNorth.getType() != Material.AIR) {
            return bNorth;
        }
        Block bWest = loc.getBlock(location.getWorld(), -add, -add);
        if (bWest.getType() != Material.AIR) {
            return bWest;
        }
        return null;
    }

    private void finishGameWin(GamePlayer winner) {
        Lang lang = plugin.lang();

        //broadcast winner
        broadcastToAll(lang.msg("chat.notify.running.finish.winner",
              new StaticReplacer().set("winner", winner.getName())));

        //finish game
        finishGame(winner);
    }

    private void finishGameTimeout() {
        Lang lang = plugin.lang();
        broadcastToAll(lang.msg("chat.notify.running.finish.timeout"));
        finishGame(null);
    }

    private void finishGame(GamePlayer winner) {
        game.switchState(new GameStateFinishing(game, winner));
    }
}
