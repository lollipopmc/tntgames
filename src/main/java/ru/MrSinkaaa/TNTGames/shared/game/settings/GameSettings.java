package ru.MrSinkaaa.TNTGames.shared.game.settings;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.model.BlockPosition;
import ru.MrSinkaaa.TNTGames.common.model.Position;
import ru.MrSinkaaa.TNTGames.shared.game.misc.GameType;
import ru.MrSinkaaa.TNTGames.shared.game.misc.Layer;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameConfig.LobbySection;
import ru.MrSinkaaa.TNTGames.shared.game.settings.GameConfig.SpawnSection;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GameSettings {

    private GameConfig config;

    private String id;
    private String name;
    private String worldName;

    private BlockPosition[] bounds;
    private int minPlayers;
    private int maxPlayers;
    private Map<Integer, Layer> layers;
    private SpawnSettings spawnSettings;
    private LobbySettings lobby;
    private WorldBorderSettings worldBorder;

    private GameType type;

    private GameSettings(String id, String worldName) {
        this.id = id;
        this.name = id;
        this.worldName = worldName;
        this.bounds = new BlockPosition[2];
        this.minPlayers = 2;
        this.maxPlayers = 16;
        this.spawnSettings = new SpawnSettings();
        this.lobby = new LobbySettings();
        this.layers = new LinkedHashMap<>();
        this.worldBorder = new WorldBorderSettings();
        onPostLoad();
    }

    private GameSettings(String id) {
        this.id = id;
        read();
        onPostLoad();
    }

    //getters
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getWorldName() {
        return worldName;
    }

    public BlockPosition[] getBounds() {
        return bounds;
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public LobbySettings getLobby() {
        return lobby;
    }

    public SpawnSettings getSpawnSettings() {
        return spawnSettings;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public WorldBorderSettings getWorldBorder() {
        return worldBorder;
    }

    public GameType getType() {
        return type;
    }

    //setters
    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
    }

    public void setBounds(BlockPosition[] bounds) {
        this.bounds = bounds;
    }

    public void setLobby(LobbySettings lobby) {
        this.lobby = lobby;
    }

    public void setSpawnSettings(SpawnSettings spawnSettings) {
        this.spawnSettings = spawnSettings;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    //func

    public GameSettingsValidation validate() {
        GameSettingsValidation result = new GameSettingsValidation(LollipopTNTGames.getInstance());

        // check : bounds not set
        if (bounds[0] == null) {
            result.addEntry("bounds-pos1");
        }

        if (bounds[1] == null) {
            result.addEntry("bounds-pos2");
        }

        //check : min players range
        if(minPlayers < 2) {
            result.addEntry("min-players-range");
        }

        if(maxPlayers < spawnSettings.spawns.size()) {
            result.addEntry("max-players-range");
        }

        lobby.validate(result);

        return result;
    }

    public void write() {
        LollipopTNTGames plugin = LollipopTNTGames.getInstance();
        if(this.config == null)
            this.config = GameConfig.create(id, worldName);

        //write
        config.name = this.name;
        config.worldName = this.worldName;

        //write : bounds
        config.bounds = new BlockPosition[2];
        System.arraycopy(this.bounds, 0, config.bounds, 0, 2);

        config.minPlayers = this.minPlayers;
        config.maxPlayers = this.maxPlayers;

        config.spawnSection = GameConfig.SpawnSection.create(config);
        this.spawnSettings.write(config.spawnSection);

        config.lobby = GameConfig.LobbySection.create(config);
        this.lobby.write(config.lobby);

        //write : worldBorder
        config.worldBorder = GameConfig.WorldBorderSection.create(config);
        this.worldBorder.write(config.worldBorder);

        config.write();
    }

    //private
    private void read() {
        LollipopTNTGames plugin = LollipopTNTGames.getInstance();
        this.config = GameConfig.load(id);

        //read
        this.name = config.name;
        this.worldName = config.worldName;

        //read : bounds
        this.bounds = new BlockPosition[2];
        System.arraycopy(config.bounds, 0, this.bounds,0,2);

        this.minPlayers = config.minPlayers;
        this.maxPlayers = config.maxPlayers;
        this.spawnSettings = new SpawnSettings();
        this.spawnSettings.read(config.spawnSection);
        this.lobby = new LobbySettings();
        this.lobby.read(config.lobby);

        this.worldBorder = new WorldBorderSettings();
        this.worldBorder.read(config.worldBorder);
    }

    private void normalizeBounds() {
        int minX = Math.min(bounds[0].getX(), bounds[1].getX());
        int minY = Math.min(bounds[0].getY(), bounds[1].getY());
        int minZ = Math.min(bounds[0].getZ(), bounds[1].getZ());

        int maxX = Math.max(bounds[0].getX(), bounds[1].getX());
        int maxY = Math.max(bounds[0].getY(), bounds[1].getY());
        int maxZ = Math.max(bounds[0].getZ(), bounds[1].getZ());

        bounds[0] = new BlockPosition(worldName, minX, minY, minZ);
        bounds[1] = new BlockPosition(worldName, maxX, maxY, maxZ);
    }

    private void onPostLoad() {
        //set game type
        switch (maxPlayers) {
            case 16:
                type = GameType.COMMON;
                break;
            case 24:
                type = GameType.PVP;
                break;
            case 32:
                type = GameType.BOW;
                break;
        }
    }

    //static

    /**
     * Create new settings instance
     *
     * @param id id
     * @param worldName world name
     * @return game settings
     */
    public static GameSettings create(String id, String worldName) {
        return new GameSettings(id,worldName);
    }

    /**
     * Load settings from file by id<br>
     * /games/{id}.yml
     *
     * @param id id
     * @return game settings
     */
    public static GameSettings load(String id) {
        return new GameSettings(id);
    }

    /**
     * Make a copy of game settings
     *
     * @param other other game settings
     * @return copy of game settings
     */
    public static GameSettings copy(GameSettings other) {
        GameSettings settings = create(other.id, other.worldName);

        settings.name = other.name;
        settings.minPlayers = other.minPlayers;
        settings.maxPlayers = other.maxPlayers;

        settings.lobby = new LobbySettings(other.lobby);
        settings.spawnSettings = new SpawnSettings(other.spawnSettings);

        return settings;
    }

    //classes

    public static class SpawnSettings {

        private List<Position> spawns;
//        private CapsulePosition capsule;
//        private ColorCapsule color;

        public SpawnSettings() {}

        public SpawnSettings(SpawnSettings other) {
            this.spawns = other.spawns;
        }

        public List<Position> getSpawns() {
            return spawns;
        }

//        public CapsulePosition getCapsule() {
//            return capsule;
//        }

//        public ColorCapsule getColor() {return color;}

        //setters

        public void setSpawns(List<Position> spawns) {
            this.spawns = spawns;
        }

//        public void setCapsule(CapsulePosition capsule) {
//            this.capsule = capsule;
//       }

//        public void setColor(ColorCapsule color) {
//            this.color = color;
//        }

        //modifiers
        public boolean addSpawn(Position position) {
            return spawns.add(position);
        }

        public Position removeSpawn() {
            if(!spawns.isEmpty())
                return spawns.remove(spawns.size()-1);
            else return null;
        }

        //func
        public void validate(GameSettingsValidation result) {
            if(spawns.isEmpty())
                result.addEntry("spawns.spawn-not-set");
        }

        public void read(SpawnSection section) {
            this.spawns = section.spawns;
        }

        public void write(SpawnSection section) {
            section.spawns = this.spawns;
        }
    }

    public static class LobbySettings {

        private Position spawn;

        public LobbySettings() {}

        public LobbySettings(LobbySettings other) {
            this.spawn = other.spawn;
        }

        //getters
        public Position getSpawn() {
            return spawn;
        }

        //setters

        public void setSpawn(Position spawn) {
            this.spawn = spawn;
        }

        //func
        public void validate(GameSettingsValidation result) {
            //check : spawn not set
            if(spawn == null)
                result.addEntry("lobby.spawn.not-set");
        }

        public void read(LobbySection section) {
            this.spawn = section.spawn;
        }

        public void write(LobbySection section) {
            section.spawn = this.spawn;
        }
    }

    public static class WorldBorderSettings {

        private int radiusFrom = 128;
        private int radiusTo = 32;

        public WorldBorderSettings() {
            //
        }

        // getters

        public int getRadiusFrom() {
            return radiusFrom;
        }

        public int getRadiusTo() {
            return radiusTo;
        }

        // setters

        public void setRadiusFrom(int radiusFrom) {
            this.radiusFrom = radiusFrom;
        }

        public void setRadiusTo(int radiusTo) {
            this.radiusTo = radiusTo;
        }

        // func

        public void read(GameConfig.WorldBorderSection section) {
            this.radiusFrom = section.radiusFrom;
            this.radiusTo = section.radiusTo;
        }

        public void write(GameConfig.WorldBorderSection section) {
            section.radiusFrom = this.radiusFrom;
            section.radiusTo = this.radiusTo;
        }
    }

}
