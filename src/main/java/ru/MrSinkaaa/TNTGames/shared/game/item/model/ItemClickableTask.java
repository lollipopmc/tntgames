package ru.MrSinkaaa.TNTGames.shared.game.item.model;

import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.jetbrains.annotations.NotNull;

public abstract class ItemClickableTask extends BukkitRunnable {

    private ItemClickable item;

    public ItemClickableTask(ItemClickable item) {
        this.item = item;
    }

    //override : BukkitRunnable
    @Override
    public synchronized @NotNull BukkitTask runTask(@NotNull Plugin plugin) throws IllegalArgumentException, IllegalStateException {
        return registerTask(super.runTask(plugin));
    }

    @Override
    public synchronized @NotNull BukkitTask runTaskLater(@NotNull Plugin plugin, long delay) throws IllegalArgumentException, IllegalStateException {
        return registerTask(super.runTaskLater(plugin, delay));
    }

    @Override
    public synchronized @NotNull BukkitTask runTaskTimer(@NotNull Plugin plugin, long delay, long period) throws IllegalArgumentException, IllegalStateException {
        return registerTask(super.runTaskTimer(plugin, delay, period));
    }

    @Override
    public synchronized  @NotNull BukkitTask runTaskAsynchronously(@NotNull Plugin plugin) throws IllegalArgumentException, IllegalStateException {
        return registerTask(super.runTaskAsynchronously(plugin));
    }

    @Override
    public synchronized void cancel() throws IllegalStateException {
        super.cancel();
        item.unregisterTask(this);
    }

    //private
    private BukkitTask registerTask(BukkitTask task) {
        item.registerTask(this);
        return task;
    }

}
