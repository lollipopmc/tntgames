package ru.MrSinkaaa.TNTGames.shared.game.settings;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class GameSettingsValidation {

    private LollipopTNTGames plugin;
    private boolean passed;
    private List<Entry> entries;

    public GameSettingsValidation(LollipopTNTGames plugin) {
        this(plugin, true);
    }

    public GameSettingsValidation(LollipopTNTGames plugin, boolean passed) {
        this.plugin = plugin;
        this.passed = passed;
        this.entries = new LinkedList<>();
    }

    // func

    public void addEntry(String messageId) {
        passed = false;
        entries.add(new Entry(plugin, messageId));
    }

    public void addEntry(String messageId, List<String> placeholders, Object ... data) {
        passed = false;
        entries.add(new Entry(plugin, messageId, placeholders, data));
    }

    // getters

    public boolean isPassed() {
        return passed;
    }

    public List<Entry> getEntries() {
        return entries;
    }

    // setters

    public void setPassed(boolean passed) {
        this.passed = passed;
    }

    // classes

    public static class Entry {

        private LollipopTNTGames plugin;

        private String messageId;
        private List<String> placeholders;
        private List<Object> data;

        public Entry(LollipopTNTGames plugin, String messageId) {
            this.plugin = plugin;
            this.messageId = "chat.notify.validation." + messageId;
            this.placeholders = new ArrayList<>();
            this.data = new ArrayList<>();
        }

        public Entry(LollipopTNTGames plugin, String messageId, List<String> placeholders, Object ... data) {
            this(plugin, messageId);
            this.placeholders = new ArrayList<>(placeholders);
            this.data = Arrays.asList(data);
        }

        // func

        public String getMessage() {
            if (!placeholders.isEmpty()) {
                StaticReplacer replacer = new StaticReplacer();
                for (int i = 0; i < data.size(); i++)
                    replacer.set(placeholders.get(i), data.get(i));
                return plugin.lang().msgRaw(messageId, replacer);
            } else {
                return plugin.lang().msgRaw(messageId);
            }
        }
    }
}
