package ru.MrSinkaaa.TNTGames.shared.item;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.common.util.FileUtils;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

public class ItemManager extends Manager {

    private Map<String, ItemInfo> items = new HashMap<>();

    public ItemManager(LollipopTNTGames plugin) {
        super(plugin);
    }

    @Override
    public void onEnable() {
        registerItems();
    }

    //getters

    public ItemInfo getItem(String id) {
        return items.get(id);
    }

    public Map<String, ItemInfo> getItems() {
        return items;
    }

    //func
    public ItemInfo getItemOrFail(String id) {
        ItemInfo info = getItem(id);
        if(info == null) {
            throw new RuntimeException("Item not found: '" + id + "'");
        }
        return info;
    }

    public void registerItems(ItemInfo info) {
        items.put(info.getId(), info);
    }

    //private
    private void registerItems() {
        Path pluginDir = plugin.getDataFolder().toPath().toAbsolutePath();
        Path itemsDir = pluginDir.resolve("configs/items");
        FileUtils.createDirectoryIfNotExists(itemsDir);
        registerItems(itemsDir);
    }

    private void registerItems(Path directory) {
        for(Path file : FileUtils.getFiles(directory)) {
            if(Files.isRegularFile(file)) {
                ItemConfig config = new ItemConfig(plugin, file.toFile());
                items.putAll(config.items);
            } else if(Files.isDirectory(file)) {
                registerItems(file);
            }
        }
    }
}
