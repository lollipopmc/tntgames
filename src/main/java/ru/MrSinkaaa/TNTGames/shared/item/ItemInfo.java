package ru.MrSinkaaa.TNTGames.shared.item;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.inventory.ItemStack;
import ru.MrSinkaaa.TNTGames.common.util.ItemUtils;

public class ItemInfo {

    private String id;
    private ItemTemplate template;
    private ConfigurationSection properties;

    public ItemInfo(String id, ItemTemplate template) {
        this(id, template, null);
    }

    public ItemInfo(String id, ItemTemplate template, ConfigurationSection properties) {
        this.id = id;
        this.template = template;
        this.properties = properties;
    }

    //getters
    public String getId() {
        return id;
    }

    public ItemTemplate getTemplate() {
        return template;
    }

    public ConfigurationSection getProperties() {
        return properties;
    }

    //static
    public static ItemInfo fromConfig(String id, ConfigurationSection section) {
        ItemStack item = section.getItemStack("item");
        ItemUtils.setId(item, id);
        String name = section.getString("name");
        String lore = section.getString("lore");
        ItemTemplate template = new ItemTemplate(item, name, lore);

        ConfigurationSection properties = section.getConfigurationSection("properties");
        return new ItemInfo(id,template, properties);
    }
}
