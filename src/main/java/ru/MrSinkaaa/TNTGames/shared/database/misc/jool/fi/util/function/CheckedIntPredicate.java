package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.IntPredicate;

@FunctionalInterface
public interface CheckedIntPredicate {

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param value the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    boolean test(int value) throws Throwable;

    /**
     * @see {@link Sneaky#intPredicate(CheckedIntPredicate)}
     */
    static IntPredicate sneaky(CheckedIntPredicate predicate) {
        return Sneaky.intPredicate(predicate);
    }

    /**
     * @see {@link Unchecked#intPredicate(CheckedIntPredicate)}
     */
    static IntPredicate unchecked(CheckedIntPredicate predicate) {
        return Unchecked.intPredicate(predicate);
    }

    /**
     * @see {@link Unchecked#intPredicate(CheckedIntPredicate, Consumer)}
     */
    static IntPredicate unchecked(CheckedIntPredicate function, Consumer<Throwable> handler) {
        return Unchecked.intPredicate(function, handler);
    }
}
