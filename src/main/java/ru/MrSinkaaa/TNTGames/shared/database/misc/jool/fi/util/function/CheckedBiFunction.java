package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.BiFunction;
import java.util.function.Consumer;

@FunctionalInterface
public interface CheckedBiFunction<T, U, R> {

    /**
     * Applies this function to the given arguments.
     *
     * @param t the first function argument
     * @param u the second function argument
     * @return the function result
     */
    R apply(T t, U u) throws Throwable;

    /**
     * @see {@link Sneaky#biFunction(CheckedBiFunction)}
     */
    static <T, U, R> BiFunction<T, U, R> sneaky(CheckedBiFunction<T, U, R> function) {
        return Sneaky.biFunction(function);
    }

    /**
     * @see {@link Unchecked#biFunction(CheckedBiFunction)}
     */
    static <T, U, R> BiFunction<T, U, R> unchecked(CheckedBiFunction<T, U, R> function) {
        return Unchecked.biFunction(function);
    }

    /**
     * @see {@link Unchecked#biFunction(CheckedBiFunction, Consumer)}
     */
    static <T, U, R> BiFunction<T, U, R> unchecked(CheckedBiFunction<T, U, R> function, Consumer<Throwable> handler) {
        return Unchecked.biFunction(function, handler);
    }
}

