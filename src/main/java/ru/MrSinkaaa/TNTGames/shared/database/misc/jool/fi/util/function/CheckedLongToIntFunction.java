package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.LongToIntFunction;

@FunctionalInterface
public interface CheckedLongToIntFunction {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    int applyAsInt(long value) throws Throwable;

    /**
     * @see {@link Sneaky#longToIntFunction(CheckedLongToIntFunction)}
     */
    static LongToIntFunction sneaky(CheckedLongToIntFunction function) {
        return Sneaky.longToIntFunction(function);
    }

    /**
     * @see {@link Unchecked#longToIntFunction(CheckedLongToIntFunction)}
     */
    static LongToIntFunction unchecked(CheckedLongToIntFunction function) {
        return Unchecked.longToIntFunction(function);
    }

    /**
     * @see {@link Unchecked#longToIntFunction(CheckedLongToIntFunction, Consumer)}
     */
    static LongToIntFunction unchecked(CheckedLongToIntFunction function, Consumer<Throwable> handler) {
        return Unchecked.longToIntFunction(function, handler);
    }
}
