package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.DoubleSupplier;

@FunctionalInterface
public interface CheckedDoubleSupplier {

    /**
     * Gets a result.
     *
     * @return a result
     */
    double getAsDouble() throws Throwable;

    /**
     * @see {@link Sneaky#doubleSupplier(CheckedDoubleSupplier)}
     */
    static DoubleSupplier sneaky(CheckedDoubleSupplier supplier) {
        return Sneaky.doubleSupplier(supplier);
    }

    /**
     * @see {@link Unchecked#doubleSupplier(CheckedDoubleSupplier)}
     */
    static DoubleSupplier unchecked(CheckedDoubleSupplier supplier) {
        return Unchecked.doubleSupplier(supplier);
    }

    /**
     * @see {@link Unchecked#doubleSupplier(CheckedDoubleSupplier, Consumer)}
     */
    static DoubleSupplier unchecked(CheckedDoubleSupplier supplier, Consumer<Throwable> handler) {
        return Unchecked.doubleSupplier(supplier, handler);
    }
}
