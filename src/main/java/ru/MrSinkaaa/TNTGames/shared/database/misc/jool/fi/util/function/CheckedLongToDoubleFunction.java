package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.LongToDoubleFunction;

@FunctionalInterface
public interface CheckedLongToDoubleFunction {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    double applyAsDouble(long value) throws Throwable;

    /**
     * @see {@link Sneaky#longToDoubleFunction(CheckedLongToDoubleFunction)}
     */
    static LongToDoubleFunction sneaky(CheckedLongToDoubleFunction function) {
        return Sneaky.longToDoubleFunction(function);
    }

    /**
     * @see {@link Unchecked#longToDoubleFunction(CheckedLongToDoubleFunction)}
     */
    static LongToDoubleFunction unchecked(CheckedLongToDoubleFunction function) {
        return Unchecked.longToDoubleFunction(function);
    }

    /**
     * @see {@link Unchecked#longToDoubleFunction(CheckedLongToDoubleFunction, Consumer)}
     */
    static LongToDoubleFunction unchecked(CheckedLongToDoubleFunction function, Consumer<Throwable> handler) {
        return Unchecked.longToDoubleFunction(function, handler);
    }
}