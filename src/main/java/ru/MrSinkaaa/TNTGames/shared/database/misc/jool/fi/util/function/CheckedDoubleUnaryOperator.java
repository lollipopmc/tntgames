package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.DoubleUnaryOperator;

@FunctionalInterface
public interface CheckedDoubleUnaryOperator {

    /**
     * Applies this operator to the given operand.
     *
     * @param operand the operand
     * @return the operator result
     */
    double applyAsDouble(double operand) throws Throwable;

    /**
     * @see {@link Sneaky#doubleUnaryOperator(CheckedDoubleUnaryOperator)}
     */
    static DoubleUnaryOperator sneaky(CheckedDoubleUnaryOperator operator) {
        return Sneaky.doubleUnaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#doubleUnaryOperator(CheckedDoubleUnaryOperator)}
     */
    static DoubleUnaryOperator unchecked(CheckedDoubleUnaryOperator operator) {
        return Unchecked.doubleUnaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#doubleUnaryOperator(CheckedDoubleUnaryOperator, Consumer)}
     */
    static DoubleUnaryOperator unchecked(CheckedDoubleUnaryOperator operator, Consumer<Throwable> handler) {
        return Unchecked.doubleUnaryOperator(operator, handler);
    }
}
