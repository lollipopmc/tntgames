package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.IntConsumer;

@FunctionalInterface
public interface CheckedIntConsumer {

    /**
     * Performs this operation on the given argument.
     *
     * @param value the input argument
     */
    void accept(int value) throws Throwable;

    /**
     * @see {@link Sneaky#intConsumer(CheckedIntConsumer)}
     */
    static IntConsumer sneaky(CheckedIntConsumer consumer) {
        return Sneaky.intConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#intConsumer(CheckedIntConsumer)}
     */
    static IntConsumer unchecked(CheckedIntConsumer consumer) {
        return Unchecked.intConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#intConsumer(CheckedIntConsumer, Consumer)}
     */
    static IntConsumer unchecked(CheckedIntConsumer consumer, Consumer<Throwable> handler) {
        return Unchecked.intConsumer(consumer, handler);
    }
}

