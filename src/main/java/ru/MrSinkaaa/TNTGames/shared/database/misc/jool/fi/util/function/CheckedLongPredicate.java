package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.LongPredicate;

@FunctionalInterface
public interface CheckedLongPredicate {

    /**
     * Evaluates this predicate on the given argument.
     *
     * @param value the input argument
     * @return {@code true} if the input argument matches the predicate,
     * otherwise {@code false}
     */
    boolean test(long value) throws Throwable;

    /**
     * @see {@link Sneaky#longPredicate(CheckedLongPredicate)}
     */
    static LongPredicate sneaky(CheckedLongPredicate predicate) {
        return Sneaky.longPredicate(predicate);
    }

    /**
     * @see {@link Unchecked#longPredicate(CheckedLongPredicate)}
     */
    static LongPredicate unchecked(CheckedLongPredicate predicate) {
        return Unchecked.longPredicate(predicate);
    }

    /**
     * @see {@link Unchecked#longPredicate(CheckedLongPredicate, Consumer)}
     */
    static LongPredicate unchecked(CheckedLongPredicate function, Consumer<Throwable> handler) {
        return Unchecked.longPredicate(function, handler);
    }
}
