package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.concurrent;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.concurrent.Callable;
import java.util.function.Consumer;

@FunctionalInterface
public interface CheckedCallable<T> {

    /**
     * Run this callable.
     */
    T call() throws Throwable;

    /**
     * @see {@link Sneaky#callable(CheckedCallable)}
     */
    static <T> Callable<T> sneaky(CheckedCallable<T> callable) {
        return Sneaky.callable(callable);
    }

    /**
     * @see {@link Unchecked#callable(CheckedCallable)}
     */
    static <T> Callable<T> unchecked(CheckedCallable<T> callable) {
        return Unchecked.callable(callable);
    }

    /**
     * @see {@link Unchecked#callable(CheckedCallable, Consumer)}
     */
    static <T> Callable<T> unchecked(CheckedCallable<T> callable, Consumer<Throwable> handler) {
        return Unchecked.callable(callable, handler);
    }
}
