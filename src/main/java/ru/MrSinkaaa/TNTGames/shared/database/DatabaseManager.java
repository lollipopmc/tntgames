package ru.MrSinkaaa.TNTGames.shared.database;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.shared.config.configs.Config;
import ru.MrSinkaaa.TNTGames.shared.database.engine.MySQL;

public class DatabaseManager extends Manager {

    private MySQL remoteDatabase;

    public DatabaseManager(LollipopTNTGames plugin) {
        super(plugin);
    }

    @Override
    public void onEnable() {
        //connect to remote database
        Config.MysqlSection mysqlSection = plugin.config().mysql;
        remoteDatabase = new MySQL(
              mysqlSection.url,
              mysqlSection.username,
              mysqlSection.password);
//        remoteDatabase.open();
    }

    @Override
    public void onDisable() {
        remoteDatabase.close();
    }

    public MySQL getRemoteDatabase() {
        return remoteDatabase;
    }
}
