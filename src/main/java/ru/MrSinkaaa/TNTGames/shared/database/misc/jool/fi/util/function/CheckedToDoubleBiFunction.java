package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ToDoubleBiFunction;

@FunctionalInterface
public interface CheckedToDoubleBiFunction<T, U> {

    /**
     * Applies this function to the given arguments.
     *
     * @param t the first function argument
     * @param u the second function argument
     * @return the function result
     */
    double applyAsDouble(T t, U u) throws Throwable;

    /**
     * @see {@link Sneaky#toDoubleBiFunction(CheckedToDoubleBiFunction)}
     */
    static <T, U> ToDoubleBiFunction<T, U> sneaky(CheckedToDoubleBiFunction<T, U> function) {
        return Sneaky.toDoubleBiFunction(function);
    }

    /**
     * @see {@link Unchecked#toDoubleBiFunction(CheckedToDoubleBiFunction)}
     */
    static <T, U> ToDoubleBiFunction<T, U> unchecked(CheckedToDoubleBiFunction<T, U> function) {
        return Unchecked.toDoubleBiFunction(function);
    }

    /**
     * @see {@link Unchecked#toDoubleBiFunction(CheckedToDoubleBiFunction, Consumer)}
     */
    static <T, U> ToDoubleBiFunction<T, U> unchecked(CheckedToDoubleBiFunction<T, U> function, Consumer<Throwable> handler) {
        return Unchecked.toDoubleBiFunction(function, handler);
    }
}

