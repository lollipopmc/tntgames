package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.Comparator;
import java.util.function.Consumer;

@FunctionalInterface
public interface CheckedComparator<T> {

    /**
     * Compares its two arguments for order.
     */
    int compare(T o1, T o2) throws Throwable;

    /**
     * @see {@link Sneaky#comparator(CheckedComparator)}
     */
    static <T> Comparator<T> sneaky(CheckedComparator<T> comparator) {
        return Sneaky.comparator(comparator);
    }

    /**
     * @see {@link Unchecked#comparator(CheckedComparator)}
     */
    static <T> Comparator<T> unchecked(CheckedComparator<T> comparator) {
        return Unchecked.comparator(comparator);
    }

    /**
     * @see {@link Unchecked#comparator(CheckedComparator, Consumer)}
     */
    static <T> Comparator<T> unchecked(CheckedComparator<T> comparator, Consumer<Throwable> handler) {
        return Unchecked.comparator(comparator, handler);
    }
}

