package ru.MrSinkaaa.TNTGames.shared.database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SQLResult implements AutoCloseable {

    private PreparedStatement statement;
    private boolean connectionCloseable;

    public SQLResult(PreparedStatement statement, boolean connectionCloseable) {
        this.statement = statement;
        this.connectionCloseable = connectionCloseable;
    }

    /**
     * Switch to next result set / update.
     *
     * @return true if it`s query (resultset), false if update(count)
     */

    public boolean next() {
        try {
            return statement.getMoreResults();
        } catch (Exception ex) {
            throw new DatabaseException("Error occured on more results request.", ex);
        }
    }

    public ResultSet getResultSet() {
        try {
            return statement.getResultSet();
        } catch (Exception ex) {
            throw new DatabaseException("Error occured on result set request.", ex);
        }
    }

    public int getUpdateCount() {
        try {
            return statement.getUpdateCount();
        } catch (Exception ex) {
            throw new DatabaseException("Error occured on result set request.", ex);
        }
    }

    public PreparedStatement getStatement() {
        return statement;
    }

    @Override
    public void close() {
        Engine.close(statement, connectionCloseable);
    }
}
