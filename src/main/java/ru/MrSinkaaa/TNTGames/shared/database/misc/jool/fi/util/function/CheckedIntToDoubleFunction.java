package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.IntToDoubleFunction;

@FunctionalInterface
public interface CheckedIntToDoubleFunction {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    double applyAsDouble(int value) throws Throwable;

    /**
     * @see {@link Sneaky#intToDoubleFunction(CheckedIntToDoubleFunction)}
     */
    static IntToDoubleFunction sneaky(CheckedIntToDoubleFunction function) {
        return Sneaky.intToDoubleFunction(function);
    }

    /**
     * @see {@link Unchecked#intToDoubleFunction(CheckedIntToDoubleFunction)}
     */
    static IntToDoubleFunction unchecked(CheckedIntToDoubleFunction function) {
        return Unchecked.intToDoubleFunction(function);
    }

    /**
     * @see {@link Unchecked#intToDoubleFunction(CheckedIntToDoubleFunction, Consumer)}
     */
    static IntToDoubleFunction unchecked(CheckedIntToDoubleFunction function, Consumer<Throwable> handler) {
        return Unchecked.intToDoubleFunction(function, handler);
    }
}
