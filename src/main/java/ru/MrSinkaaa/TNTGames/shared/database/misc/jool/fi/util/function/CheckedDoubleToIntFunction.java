package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.DoubleToIntFunction;

@FunctionalInterface
public interface CheckedDoubleToIntFunction {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    int applyAsInt(double value) throws Throwable;

    /**
     * @see {@link Sneaky#doubleToIntFunction(CheckedDoubleToIntFunction)}
     */
    static DoubleToIntFunction sneaky(CheckedDoubleToIntFunction function) {
        return Sneaky.doubleToIntFunction(function);
    }

    /**
     * @see {@link Unchecked#doubleToIntFunction(CheckedDoubleToIntFunction)}
     */
    static DoubleToIntFunction unchecked(CheckedDoubleToIntFunction function) {
        return Unchecked.doubleToIntFunction(function);
    }

    /**
     * @see {@link Unchecked#doubleToIntFunction(CheckedDoubleToIntFunction, Consumer)}
     */
    static DoubleToIntFunction unchecked(CheckedDoubleToIntFunction function, Consumer<Throwable> handler) {
        return Unchecked.doubleToIntFunction(function, handler);
    }
}
