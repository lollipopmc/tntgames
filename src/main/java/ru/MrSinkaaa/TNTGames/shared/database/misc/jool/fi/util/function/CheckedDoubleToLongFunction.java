package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.DoubleToLongFunction;

@FunctionalInterface
public interface CheckedDoubleToLongFunction {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    long applyAsLong(double value) throws Throwable;

    /**
     * @see {@link Sneaky#doubleToLongFunction(CheckedDoubleToLongFunction)}
     */
    static DoubleToLongFunction sneaky(CheckedDoubleToLongFunction function) {
        return Sneaky.doubleToLongFunction(function);
    }

    /**
     * @see {@link Unchecked#doubleToLongFunction(CheckedDoubleToLongFunction)}
     */
    static DoubleToLongFunction unchecked(CheckedDoubleToLongFunction function) {
        return Unchecked.doubleToLongFunction(function);
    }

    /**
     * @see {@link Unchecked#doubleToLongFunction(CheckedDoubleToLongFunction, Consumer)}
     */
    static DoubleToLongFunction unchecked(CheckedDoubleToLongFunction function, Consumer<Throwable> handler) {
        return Unchecked.doubleToLongFunction(function, handler);
    }
}
