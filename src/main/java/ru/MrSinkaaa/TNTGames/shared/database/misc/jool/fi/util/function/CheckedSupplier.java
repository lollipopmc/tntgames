package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.Supplier;

@FunctionalInterface
public interface CheckedSupplier<T> {

    /**
     * Gets a result.
     *
     * @return a result
     */
    T get() throws Throwable;

    /**
     * @see {@link Sneaky#supplier(CheckedSupplier)}
     */
    static <T> Supplier<T> sneaky(CheckedSupplier<T> supplier) {
        return Sneaky.supplier(supplier);
    }

    /**
     * @see {@link Unchecked#supplier(CheckedSupplier)}
     */
    static <T> Supplier<T> unchecked(CheckedSupplier<T> supplier) {
        return Unchecked.supplier(supplier);
    }

    /**
     * @see {@link Unchecked#supplier(CheckedSupplier, Consumer)}
     */
    static <T> Supplier<T> unchecked(CheckedSupplier<T> supplier, Consumer<Throwable> handler) {
        return Unchecked.supplier(supplier, handler);
    }
}
