package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ToIntBiFunction;

@FunctionalInterface
public interface CheckedToIntBiFunction<T, U> {

    /**
     * Applies this function to the given arguments.
     *
     * @param t the first function argument
     * @param u the second function argument
     * @return the function result
     */
    int applyAsInt(T t, U u) throws Throwable;

    /**
     * @see {@link Sneaky#toIntBiFunction(CheckedToIntBiFunction)}
     */
    static <T, U> ToIntBiFunction<T, U> sneaky(CheckedToIntBiFunction<T, U> function) {
        return Sneaky.toIntBiFunction(function);
    }

    /**
     * @see {@link Unchecked#toIntBiFunction(CheckedToIntBiFunction)}
     */
    static <T, U> ToIntBiFunction<T, U> unchecked(CheckedToIntBiFunction<T, U> function) {
        return Unchecked.toIntBiFunction(function);
    }

    /**
     * @see {@link Unchecked#toIntBiFunction(CheckedToIntBiFunction, Consumer)}
     */
    static <T, U> ToIntBiFunction<T, U> unchecked(CheckedToIntBiFunction<T, U> function, Consumer<Throwable> handler) {
        return Unchecked.toIntBiFunction(function, handler);
    }
}
