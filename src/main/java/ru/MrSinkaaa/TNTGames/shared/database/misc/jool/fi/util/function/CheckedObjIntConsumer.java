package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ObjIntConsumer;

@FunctionalInterface
public interface CheckedObjIntConsumer<T> {

    /**
     * Performs this operation on the given arguments.
     *
     * @param t the first input argument
     * @param value the second input argument
     */
    void accept(T t, int value) throws Throwable;

    /**
     * @see {@link Sneaky#objIntConsumer(CheckedObjIntConsumer)}
     */
    static <T> ObjIntConsumer<T> sneaky(CheckedObjIntConsumer<T> consumer) {
        return Sneaky.objIntConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#objIntConsumer(CheckedObjIntConsumer)}
     */
    static <T> ObjIntConsumer<T> unchecked(CheckedObjIntConsumer<T> consumer) {
        return Unchecked.objIntConsumer(consumer);
    }

    /**
     * @see {@link Unchecked#objIntConsumer(CheckedObjIntConsumer, Consumer)}
     */
    static <T> ObjIntConsumer<T> unchecked(CheckedObjIntConsumer<T> consumer, Consumer<Throwable> handler) {
        return Unchecked.objIntConsumer(consumer, handler);
    }
}
