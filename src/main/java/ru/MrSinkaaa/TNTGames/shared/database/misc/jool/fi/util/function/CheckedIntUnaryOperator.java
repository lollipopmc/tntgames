package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.IntUnaryOperator;

@FunctionalInterface
public interface CheckedIntUnaryOperator {

    /**
     * Applies this operator to the given operand.
     *
     * @param operand the operand
     * @return the operator result
     */
    int applyAsInt(int operand) throws Throwable;

    /**
     * @see {@link Sneaky#intUnaryOperator(CheckedIntUnaryOperator)}
     */
    static IntUnaryOperator sneaky(CheckedIntUnaryOperator operator) {
        return Sneaky.intUnaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#intUnaryOperator(CheckedIntUnaryOperator)}
     */
    static IntUnaryOperator unchecked(CheckedIntUnaryOperator operator) {
        return Unchecked.intUnaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#intUnaryOperator(CheckedIntUnaryOperator, Consumer)}
     */
    static IntUnaryOperator unchecked(CheckedIntUnaryOperator operator, Consumer<Throwable> handler) {
        return Unchecked.intUnaryOperator(operator, handler);
    }
}
