package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.IntToLongFunction;

@FunctionalInterface
public interface CheckedIntToLongFunction {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    long applyAsLong(int value) throws Throwable;

    /**
     * @see {@link Sneaky#intToLongFunction(CheckedIntToLongFunction)}
     */
    static IntToLongFunction sneaky(CheckedIntToLongFunction function) {
        return Sneaky.intToLongFunction(function);
    }

    /**
     * @see {@link Unchecked#intToLongFunction(CheckedIntToLongFunction)}
     */
    static IntToLongFunction unchecked(CheckedIntToLongFunction function) {
        return Unchecked.intToLongFunction(function);
    }

    /**
     * @see {@link Unchecked#intToLongFunction(CheckedIntToLongFunction, Consumer)}
     */
    static IntToLongFunction unchecked(CheckedIntToLongFunction function, Consumer<Throwable> handler) {
        return Unchecked.intToLongFunction(function, handler);
    }
}

