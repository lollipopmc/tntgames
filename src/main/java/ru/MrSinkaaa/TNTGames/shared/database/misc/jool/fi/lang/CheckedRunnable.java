package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.lang;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;

@FunctionalInterface
public interface CheckedRunnable {

    /**
     * Run this runnable.
     */
    void run() throws Throwable;

    /**
     * @see {@link Sneaky#runnable(CheckedRunnable)}
     */
    static Runnable sneaky(CheckedRunnable runnable) {
        return Sneaky.runnable(runnable);
    }

    /**
     * @see {@link Unchecked#runnable(CheckedRunnable)}
     */
    static Runnable unchecked(CheckedRunnable runnable) {
        return Unchecked.runnable(runnable);
    }

    /**
     * @see {@link Unchecked#runnable(CheckedRunnable, Consumer)}
     */
    static Runnable unchecked(CheckedRunnable runnable, Consumer<Throwable> handler) {
        return Unchecked.runnable(runnable, handler);
    }
}

