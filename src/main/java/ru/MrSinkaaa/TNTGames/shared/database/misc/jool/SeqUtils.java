package ru.MrSinkaaa.TNTGames.shared.database.misc.jool;

import java.util.Iterator;
import java.util.OptionalLong;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Predicate;

class SeqUtils {

    static <T> OptionalLong indexOf(Iterator<T> iterator, Predicate<? super T> predicate) {
        for (long index = 0; iterator.hasNext(); index++)
            if (predicate.test(iterator.next()))
                return OptionalLong.of(index);

        return OptionalLong.empty();
    }

    /**
     * Sneaky throw any type of Throwable.
     */
    static void sneakyThrow(Throwable throwable) {
        SeqUtils.<RuntimeException>sneakyThrow0(throwable);
    }

    /**
     * Sneaky throw any type of Throwable.
     */
    @SuppressWarnings("unchecked")
    static <E extends Throwable> void sneakyThrow0(Throwable throwable) throws E {
        throw (E) throwable;
    }

    @FunctionalInterface
    interface DelegatingSpliterator<T, U> {
        boolean tryAdvance(Spliterator<? extends T> delegate, Consumer<? super U> action);
    }

    static Runnable closeAll(AutoCloseable... closeables) {
        return () -> {
            Throwable t = null;

            for (AutoCloseable closeable : closeables) {
                try {
                    closeable.close();
                }
                catch (Throwable t1) {
                    if (t == null)
                        t = t1;
                    else
                        t.addSuppressed(t1);
                }
            }

            if (t != null)
                sneakyThrow(t);
        };
    }
}