package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;

@FunctionalInterface
public interface CheckedConsumer<T> {

    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    void accept(T t) throws Throwable;

    /**
     * @see {@link Sneaky#consumer(CheckedConsumer)}
     */
    static <T> Consumer<T> sneaky(CheckedConsumer<T> consumer) {
        return Sneaky.consumer(consumer);
    }

    /**
     * @see {@link Unchecked#consumer(CheckedConsumer)}
     */
    static <T> Consumer<T> unchecked(CheckedConsumer<T> consumer) {
        return Unchecked.consumer(consumer);
    }

    /**
     * @see {@link Unchecked#consumer(CheckedConsumer, Consumer)}
     */
    static <T> Consumer<T> unchecked(CheckedConsumer<T> consumer, Consumer<Throwable> handler) {
        return Unchecked.consumer(consumer, handler);
    }
}
