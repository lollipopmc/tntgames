package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ToLongBiFunction;

@FunctionalInterface
public interface CheckedToLongBiFunction<T, U> {

    /**
     * Applies this function to the given arguments.
     *
     * @param t the first function argument
     * @param u the second function argument
     * @return the function result
     */
    long applyAsLong(T t, U u) throws Throwable;

    /**
     * @see {@link Sneaky#toLongBiFunction(CheckedToLongBiFunction)}
     */
    static <T, U> ToLongBiFunction<T, U> sneaky(CheckedToLongBiFunction<T, U> function) {
        return Sneaky.toLongBiFunction(function);
    }

    /**
     * @see {@link Unchecked#toLongBiFunction(CheckedToLongBiFunction)}
     */
    static <T, U> ToLongBiFunction<T, U> unchecked(CheckedToLongBiFunction<T, U> function) {
        return Unchecked.toLongBiFunction(function);
    }

    /**
     * @see {@link Unchecked#toLongBiFunction(CheckedToLongBiFunction, Consumer)}
     */
    static <T, U> ToLongBiFunction<T, U> unchecked(CheckedToLongBiFunction<T, U> function, Consumer<Throwable> handler) {
        return Unchecked.toLongBiFunction(function, handler);
    }
}
