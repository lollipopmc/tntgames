package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.ToLongFunction;

@FunctionalInterface
public interface CheckedToLongFunction<T> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    long applyAsLong(T value) throws Throwable;

    /**
     * @see {@link Sneaky#toLongFunction(CheckedToLongFunction)}
     */
    static <T> ToLongFunction<T> sneaky(CheckedToLongFunction<T> function) {
        return Sneaky.toLongFunction(function);
    }

    /**
     * @see {@link Unchecked#toLongFunction(CheckedToLongFunction)}
     */
    static <T> ToLongFunction<T> unchecked(CheckedToLongFunction<T> function) {
        return Unchecked.toLongFunction(function);
    }

    /**
     * @see {@link Unchecked#toLongFunction(CheckedToLongFunction, Consumer)}
     */
    static <T> ToLongFunction<T> unchecked(CheckedToLongFunction<T> function, Consumer<Throwable> handler) {
        return Unchecked.toLongFunction(function, handler);
    }
}
