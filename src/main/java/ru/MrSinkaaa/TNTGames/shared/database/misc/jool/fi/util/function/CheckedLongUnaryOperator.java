package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.LongUnaryOperator;

@FunctionalInterface
public interface CheckedLongUnaryOperator {

    /**
     * Applies this operator to the given operand.
     *
     * @param operand the operand
     * @return the operator result
     */
    long applyAsLong(long operand) throws Throwable;

    /**
     * @see {@link Sneaky#longUnaryOperator(CheckedLongUnaryOperator)}
     */
    static LongUnaryOperator sneaky(CheckedLongUnaryOperator operator) {
        return Sneaky.longUnaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#longUnaryOperator(CheckedLongUnaryOperator)}
     */
    static LongUnaryOperator unchecked(CheckedLongUnaryOperator operator) {
        return Unchecked.longUnaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#longUnaryOperator(CheckedLongUnaryOperator, Consumer)}
     */
    static LongUnaryOperator unchecked(CheckedLongUnaryOperator operator, Consumer<Throwable> handler) {
        return Unchecked.longUnaryOperator(operator, handler);
    }
}

