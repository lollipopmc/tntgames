package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.LongFunction;

@FunctionalInterface
public interface CheckedLongFunction<R> {

    /**
     * Applies this function to the given argument.
     *
     * @param value the function argument
     * @return the function result
     */
    R apply(long value) throws Throwable;

    /**
     * @see {@link Sneaky#longFunction(CheckedLongFunction)}
     */
    static <R> LongFunction<R> sneaky(CheckedLongFunction<R> function) {
        return Sneaky.longFunction(function);
    }

    /**
     * @see {@link Unchecked#longFunction(CheckedLongFunction)}
     */
    static <R> LongFunction<R> unchecked(CheckedLongFunction<R> function) {
        return Unchecked.longFunction(function);
    }

    /**
     * @see {@link Unchecked#longFunction(CheckedLongFunction, Consumer)}
     */
    static <R> LongFunction<R> unchecked(CheckedLongFunction<R> function, Consumer<Throwable> handler) {
        return Unchecked.longFunction(function, handler);
    }
}
