package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.Function;

@FunctionalInterface
public interface CheckedFunction<T, R> {

    /**
     * Applies this function to the given argument.
     *
     * @param t the function argument
     * @return the function result
     */
    R apply(T t) throws Throwable;

    /**
     * @see {@link Sneaky#function(CheckedFunction)}
     */
    static <T, R> Function<T, R> sneaky(CheckedFunction<T, R> function) {
        return Sneaky.function(function);
    }

    /**
     * @see {@link Unchecked#function(CheckedFunction)}
     */
    static <T, R> Function<T, R> unchecked(CheckedFunction<T, R> function) {
        return Unchecked.function(function);
    }

    /**
     * @see {@link Unchecked#function(CheckedFunction, Consumer)}
     */
    static <T, R> Function<T, R> unchecked(CheckedFunction<T, R> function, Consumer<Throwable> handler) {
        return Unchecked.function(function, handler);
    }
}

