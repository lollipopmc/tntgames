package ru.MrSinkaaa.TNTGames.shared.database.misc.jool.fi.util.function;

import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Sneaky;
import ru.MrSinkaaa.TNTGames.shared.database.misc.jool.Unchecked;

import java.util.function.Consumer;
import java.util.function.LongBinaryOperator;

@FunctionalInterface
public interface CheckedLongBinaryOperator {

    /**
     * Applies this operator to the given operands.
     *
     * @param left the first operand
     * @param right the second operand
     * @return the operator result
     */
    long applyAsLong(long left, long right) throws Throwable;

    /**
     * @see {@link Sneaky#longBinaryOperator(CheckedLongBinaryOperator)}
     */
    static LongBinaryOperator sneaky(CheckedLongBinaryOperator operator) {
        return Sneaky.longBinaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#longBinaryOperator(CheckedLongBinaryOperator)}
     */
    static LongBinaryOperator unchecked(CheckedLongBinaryOperator operator) {
        return Unchecked.longBinaryOperator(operator);
    }

    /**
     * @see {@link Unchecked#longBinaryOperator(CheckedLongBinaryOperator, Consumer)}
     */
    static LongBinaryOperator unchecked(CheckedLongBinaryOperator operator, Consumer<Throwable> handler) {
        return Unchecked.longBinaryOperator(operator, handler);
    }
}

