package ru.MrSinkaaa.TNTGames.common.replacer.replacers;

import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CombinedReplacer extends Replacer {

    private List<Replacer> replacers = new ArrayList<>();

    public CombinedReplacer(Replacer... replacers) {
        this.replacers.addAll(Arrays.asList(replacers));
    }

    // modifiers

    public void addReplacer(Replacer replacer) {
        replacers.add(replacer);
    }

    // func

    @Override
    public String apply(String str) {
        for (Replacer replacer : replacers)
            str = replacer.apply(str);
        return str;
    }

    @Override
    public String apply(String str, Object context) {
        for (Replacer replacer : replacers)
            str = replacer.apply(str, context);
        return str;
    }
}
