package ru.MrSinkaaa.TNTGames.common.replacer.replacers;

import ru.MrSinkaaa.TNTGames.common.replacer.Replacer;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

public class DynamicReplacer<U> extends Replacer {

    private Map<String, Function<U, Object>> contextMapping = new HashMap<>();
    private Map<String, Supplier<Object>> staticMapping = new HashMap<>();

    public DynamicReplacer() {
        //
    }

    // func

    @Override
    public String apply(String str) {
        return apply(str, null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public String apply(String str, Object context) {
        Map<String, Object> staticMap = new HashMap<>();

        // apply static mapping
        staticMapping.forEach((placeholder, supplier) ->
              staticMap.put(placeholder, String.valueOf(supplier.get())));

        // apply context mapping
        U ctx;
        try { ctx = (U) context; }
        catch (ClassCastException ex) { return str; }

        contextMapping.forEach((placeholder, fun) ->
              staticMap.put(placeholder, String.valueOf(fun.apply(ctx))));
        return replace(str, staticMap);
    }

    // modifiers

    public DynamicReplacer<U> set(String placeholder, Function<U, Object> replacement) {
        contextMapping.put(placeholder, replacement);
        return this;
    }

    public DynamicReplacer<U> set(String placeholder, Supplier<Object> replacement) {
        staticMapping.put(placeholder, replacement);
        return this;
    }
}
