package ru.MrSinkaaa.TNTGames.common.util.model;

/**
 *
 * @author MrSinkaaa
 */
public class TextProgressBar {
    
    private int value;
    private int max;
    
    private int scale;
    private int scaleValue;
    
    private String firstPrefix;
    private String firstCharacter;
    
    private String lastPrefix;
    private String lastCharacter;

    public TextProgressBar(int scale, int max, String firstCharacter, String lastCharacter) {
        this(scale, max, "", "", firstCharacter, lastCharacter);
    }

    public TextProgressBar(int scale, int max, String firstPrefix, String firstCharacter, String lastPrefix, String lastCharacter) {
        this.value = 0;
        this.max = max;
        this.scale = scale;

        this.firstPrefix = firstPrefix;
        this.firstCharacter = firstCharacter;
        
        this.lastPrefix = lastPrefix;
        this.lastCharacter = lastCharacter;
    }
    
    public int getValue() {
        return value;
    }
    
    public boolean updateValue(int value) {
        this.value = value;
        
        int newScaleValue = (int) ((double)value / max * scale);
        boolean changed = newScaleValue != scaleValue;
        scaleValue = newScaleValue;
        return changed;
    }
    
    public String build() {
        StringBuilder builder = new StringBuilder();
        
        if(scaleValue > 0) {
            builder.append(firstPrefix);
            builder.append(lastCharacter.repeat(scaleValue));
        }
        
        if(scaleValue < scale) {
            int amount = scale - scaleValue;
            builder.append(lastPrefix);
            builder.append(lastCharacter.repeat(amount));
        }
        return builder.toString();
    }
    
}
