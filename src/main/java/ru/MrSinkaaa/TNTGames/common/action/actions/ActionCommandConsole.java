/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.MrSinkaaa.TNTGames.common.action.actions;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.common.action.Action;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;

/**
 *
 * @author MrSinkaaa
 */
public class ActionCommandConsole extends Action {
    
    private String command;

    public ActionCommandConsole(String command) {
        this.command = command;
    }

    @Override
    public void execute(Player p) {
        String commandToExecute = command;
        if(p != null) {
            commandToExecute = new StaticReplacer().set("player", p.getName()).apply(command);
        }
        Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), command);
    }
    
    public static ActionCommandConsole fromConfig(ConfigurationSection section) {
        String command = section.getString("command");
        return new ActionCommandConsole(command);
    }
    
}
