/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.MrSinkaaa.TNTGames.common.action.actions;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.common.action.Action;

/**
 *
 * @author MrSinkaaa
 */
public class ActionKick extends Action {
    
    private String message;

    public ActionKick(String message) {
        this.message = message;
    }

    @Override
    public void execute(Player p) {
        Bukkit.getScheduler().runTaskLater(plugin, () -> 
            p.kickPlayer(message), 1L);
    }
    
    public static ActionKick fromConfig(ConfigurationSection section) {
        String message = section.getString("message");
        return new ActionKick(message);
    }
    
}
