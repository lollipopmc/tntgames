/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.MrSinkaaa.TNTGames.common.action;


import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.action.actions.ActionCommandConsole;
import ru.MrSinkaaa.TNTGames.common.action.actions.ActionCommandPlayer;
import ru.MrSinkaaa.TNTGames.common.action.actions.ActionKick;
import ru.MrSinkaaa.TNTGames.common.action.actions.ActionNone;

/**
 *
 * @author MrSinkaaa
 */
public abstract class Action {
    
    protected LollipopTNTGames plugin;
    
    public Action() {
        plugin = LollipopTNTGames.getInstance();
    }
    
    public abstract void execute(Player p);
    
    public static Action fromConfig(ConfigurationSection section) {
        if(section == null) return new ActionNone();
        if(!section.contains("type")) return new ActionNone();
        
        ActionType type = ActionType.get(section.getString("type"));
        switch(type) {
            case PLAYER_COMMAND:
                return ActionCommandPlayer.fromConfig(section);
            case CONSOLE_COMMAND:
                return ActionCommandConsole.fromConfig(section);
            case KICK:
                return ActionKick.fromConfig(section);
            default:
                return new ActionNone();
        }   
    }
}
