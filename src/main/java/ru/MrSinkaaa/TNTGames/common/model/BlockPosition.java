package ru.MrSinkaaa.TNTGames.common.model;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.NumberConversions;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;

import java.util.Objects;

public class BlockPosition {

    private String worldName;
    private int x;
    private int y;
    private int z;

    public BlockPosition(BlockPosition other) {
        this(other.worldName, other.x, other.y, other.z);
    }

    public BlockPosition(String worldName, int x, int y, int z) {
        this.worldName = worldName;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    // func

    public Location toLocation() {
        World world = Bukkit.getWorld(worldName);
        if (world == null) throw new RuntimeException("World " + worldName + " not loaded.");
        return new Location(world, x, y, z);
    }

    public Position toPosition() {
        return new Position(worldName, x, y, z);
    }

    public BlockPosition add(int x, int y, int z) {
        this.x += x;
        this.y += y;
        this.z += z;
        return this;
    }

    public BlockPosition substract(int x, int y, int z) {
        return add(-x, -y, -z);
    }

    public double distance(BlockPosition position) {
        return Math.sqrt(distanceSquared(position));
    }

    public double distanceSquared(BlockPosition position) {
        if (!worldName.equals(position.worldName)) return 0;
        return NumberConversions.square(x - position.x)
              + NumberConversions.square(y - position.y)
              + NumberConversions.square(z - position.z);
    }

    @SuppressWarnings("MethodDoesntCallSuperMethod")
    public BlockPosition clone() {
        return new BlockPosition(this);
    }

    public String serialize() {
        return String.format("%s %d %d %d", worldName, x, y, z);
    }

    // getters

    public String getWorldName() {
        return worldName;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    // object

    @Override
    public String toString() {
        return "{" + serialize() + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BlockPosition)) return false;
        BlockPosition that = (BlockPosition) o;
        return
              x == that.x &&
                    y == that.y &&
                    z == that.z &&
                    worldName.equals(that.worldName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(worldName, x, y, z);
    }

    // static

    public static BlockPosition fromLocation(Location location) {
        String worldName = location.getWorld().getName();
        int x = toBlockCoord(location.getX());
        int y = toBlockCoord(location.getY());
        int z = toBlockCoord(location.getZ());

        return new BlockPosition(worldName, x, y, z);
    }

    public static BlockPosition deserialize(String str) {
        try {
            String[] data = str.split(" ");

            String worldName = data[0];
            int x = Integer.parseInt(data[1]);
            int y = Integer.parseInt(data[2]);
            int z = Integer.parseInt(data[3]);

            return new BlockPosition(worldName, x, y, z);
        } catch (Exception ex) {
            MiscUtils.warning("Failed to deserialize block position '%s'.", str);
            throw new RuntimeException(ex);
        }
    }

    private static int toBlockCoord(double coord) {
        final int floor = (int) coord;
        return floor == coord ? floor : floor - (int) (Double.doubleToRawLongBits(coord) >>> 63);
    }
}
