package ru.MrSinkaaa.TNTGames.common.debug;

import ru.MrSinkaaa.TNTGames.LollipopTNTGames;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.shared.config.configs.Config;

import java.util.EnumMap;
import java.util.Map;

/**
 *
 * @author MrSinkaaa
 */
public class DebugManager extends Manager {
    
    private Map<DebugType, String> debugTypesMap;
    
    public DebugManager(LollipopTNTGames plugin) {
        super(plugin);
    }
    
    @Override
    public void onEnable() {
        loadSettings();
    }
    
    @Override
    public void onReload() {
        loadSettings();
    }
    
    // func
    
    public void info(DebugType type, String message, Object... args) {
        String prefix = debugTypesMap.get(type);
        if (prefix != null)
            MiscUtils.info(prefix + message, args);
    }
    
    public void warning(DebugType type, String message, Object... args) {
        String prefix = debugTypesMap.get(type);
        if (prefix != null)
            MiscUtils.warning(prefix + message, args);
    }
    
    // private
    
    private void loadSettings() {
        Config config = plugin.config();
        debugTypesMap = new EnumMap<>(DebugType.class);
        for (String typeName : config.debug) {
            DebugType type = DebugType.get(typeName);
            if (type != null) {
                String prefix = "[" + type.name().toLowerCase() + "] ";
                debugTypesMap.put(type, prefix);
            }
        }
    }
}
