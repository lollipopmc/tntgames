package ru.MrSinkaaa.TNTGames;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import ru.MrSinkaaa.TNTGames.common.Manager;
import ru.MrSinkaaa.TNTGames.common.debug.DebugManager;
import ru.MrSinkaaa.TNTGames.common.debug.DebugType;
import ru.MrSinkaaa.TNTGames.common.replacer.replacers.StaticReplacer;
import ru.MrSinkaaa.TNTGames.common.util.MiscUtils;
import ru.MrSinkaaa.TNTGames.shared.command.commands.CmdNextGame;
import ru.MrSinkaaa.TNTGames.shared.command.commands.CmdStats;
import ru.MrSinkaaa.TNTGames.shared.command.commands.CmdTNTGames;
import ru.MrSinkaaa.TNTGames.shared.config.ConfigObject;
import ru.MrSinkaaa.TNTGames.shared.config.Lang;
import ru.MrSinkaaa.TNTGames.shared.config.configs.Config;
import ru.MrSinkaaa.TNTGames.shared.database.DatabaseManager;
import ru.MrSinkaaa.TNTGames.shared.game.GameManager;
import ru.MrSinkaaa.TNTGames.shared.game.GamePlayer;
import ru.MrSinkaaa.TNTGames.shared.item.ItemManager;
import ru.MrSinkaaa.TNTGames.shared.menu.MenuManager;
import ru.MrSinkaaa.TNTGames.shared.misc.PlaceholderHook;
import ru.MrSinkaaa.TNTGames.shared.schematic.SchematicManager;
import ru.MrSinkaaa.TNTGames.shared.schematic.SchematicManagerFAWE;
import ru.MrSinkaaa.TNTGames.shared.schematic.SchematicManagerWE;
import ru.MrSinkaaa.TNTGames.shared.sideboard.SideboardManager;
import ru.MrSinkaaa.TNTGames.shared.sync.SyncManager;
import ru.MrSinkaaa.TNTGames.shared.world.WorldManager;
import ua.i0xhex.lib.command.CommandManager;
import ua.i0xhex.lib.command.exceptions.InvalidSyntaxException;
import ua.i0xhex.lib.command.exceptions.NoPermissionException;
import ua.i0xhex.lib.command.execution.CommandExecutionCoordinator;
import ua.i0xhex.lib.command.minecraft.extras.MinecraftExceptionHandler;
import ua.i0xhex.lib.command.paper.PaperCommandManager;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

/**
 * @author MrSinkaaa
 */
public class LollipopTNTGames extends JavaPlugin {

    private static LollipopTNTGames INSTANCE;

    private Map<Class<?>, ConfigObject> configs;
    private Map<Class<?>, Manager> managers;

    private Economy economy;
    private Permission permission;

    public LollipopTNTGames() {
        INSTANCE = this;
    }

    @Override
    public void onEnable() {
        try {

            //providers
//            economy = Bukkit.getServicesManager().getRegistration(Economy.class).getProvider();
            permission = Bukkit.getServicesManager().getRegistration(Permission.class).getProvider();
            Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

            //configs
            configs = new HashMap<>();
            registerConfig(Lang.load());
            registerConfig(Config.load());

            //managers
            managers = new HashMap<>();
            enableManager(new DebugManager(this));

            Manager manager;
            if (config().useFawe) {
                MiscUtils.info("Using FAWE to operate world.");
                manager = new SchematicManagerFAWE(this);
            } else {
                MiscUtils.info("Using WE to operate world.");
                manager = new SchematicManagerWE(this);
            }
            enableManager(SchematicManager.class, manager);

            enableManager(new DatabaseManager(this));
            enableManager(new WorldManager(this));
            enableManager(new ItemManager(this));
            enableManager(new SideboardManager(this));
//            enableManager(new PointsManager(this));

            enableManager(new MenuManager(this));
            enableManager(new GameManager(this));

            enableManager(new SyncManager(this));

            if (Bukkit.getPluginManager().isPluginEnabled("PlaceholderAPI")) {
                new PlaceholderHook(this).register();
            }

            //commands
            registerCommands();
        } catch (Exception ex) {
            MiscUtils.warning("Failed to enable plugin. Disabling..");
            ex.printStackTrace();
            Bukkit.getPluginManager().disablePlugin(this);
        }
    }

    @Override
    public void onDisable() {
//        disableManager(PointsManager.class);
        disableManager(DatabaseManager.class);
    }

    public Lang lang() {
        return getConfig(Lang.class);
    }

    public Config config() {
        return getConfig(Config.class);
    }

    public <T> T getConfig(Class<T> type) {
        return type.cast(configs.get(type));
    }

    public <T> T getManager(Class<T> type) {
        return type.cast(managers.get(type));
    }

    public Economy getEconomy() {
        return economy;
    }

    public Permission getPermission() {
        return permission;
    }

    public static LollipopTNTGames getInstance() {
        return INSTANCE;
    }

    public GamePlayer getPlayer(Player p) {
        if (p == null) {
            return null;
        }
        return getManager(GameManager.class).getPlayer(p);
    }

    public Collection<GamePlayer> getPlayers() {
        return getManager(GameManager.class).getPlayers().values();
    }

    public <T extends Manager> T enableManager(T manager) {
        return enableManager(manager.getClass(), manager);
    }

    public <T extends Manager> T enableManager(Class<?> type, T manager) {
        managers.put(type, manager);
        manager.onEnable();
        return manager;
    }

    public void disableManager(Class<?> type) {
        try {
            Manager manager = managers.remove(type);
            if (manager != null) {
                manager.onDisable();
            }
        } catch (Exception ex) {
            MiscUtils.exception(ex);
        }
    }

    private void registerConfig(ConfigObject config) {
        configs.put(config.getClass(), config);
    }

    private void registerCommands() {
        try {
            CommandManager<CommandSender> manager = setupCommandManager();
            new CmdTNTGames(this, manager);
            new CmdStats(this, manager);
            new CmdNextGame(this, manager);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private CommandManager<CommandSender> setupCommandManager() throws Exception {
        CommandManager<CommandSender> manager = new PaperCommandManager<>
              (this, CommandExecutionCoordinator.simpleCoordinator(), Function.identity(), Function.identity());

        Lang lang = lang();
        new MinecraftExceptionHandler<CommandSender>().withDefaultHandlers().withHandler(MinecraftExceptionHandler.ExceptionType.NO_PERMISSION, (sender, exception) -> {
            NoPermissionException ex = (NoPermissionException) exception;
            return lang.msg("chat.command._.no-permission", new StaticReplacer().set("permission", ex.getMissingPermission()));
        }).withHandler(MinecraftExceptionHandler.ExceptionType.INVALID_SYNTAX, (sender, exception) -> {
            InvalidSyntaxException ex = (InvalidSyntaxException) exception;
            return lang.msg("chat.command._.syntax", new StaticReplacer().set("syntax", ex.getCorrectSyntax()));
        }).withHandler(MinecraftExceptionHandler.ExceptionType.INVALID_SENDER, (sender, exception) -> {
            return lang.msg("chat.command._.player-only");
        }).apply(manager, s -> s);

        return manager;
    }

    public static void debugInfo(DebugType type, String message, Object... args) {
        DebugManager manager = getInstance().getManager(DebugManager.class);
        manager.info(type, message, args);
    }

    public static void debugWarning(DebugType type, String message, Object... args) {
        DebugManager manager = getInstance().getManager(DebugManager.class);
        manager.warning(type, message, args);
    }
}
